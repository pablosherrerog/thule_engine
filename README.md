# README
The goal for this project is to create a small game engine to practice and experiment. To start I will be using the [learnopengl tutorials](https://learnopengl.com/), [Austin Morlan ECS introduction](https://austinmorlan.com/posts/entity_component_system/#demo) and The Cherno [Game Engine series](https://www.youtube.com/playlist?list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT).


**Default scene example**
!["Default scene example"](others/exampleTestScene.jpg)

![Example rotating camera and adding component](https://drive.google.com/uc?id=1ka-TYLnzmnaYHZd0AUskaEf-iQqBihQp)


## Start
This project has been tested on Windows 10.

### LFS
Some files are uploaded using lfs, before pull install LFS    
``git lfs install``
### Submodules
Download all submodules (glad, assimp, glfw...)  
``git submodule update --init --recursive``

### How to compile it
In case you are using vscode the following plugins are needed:
- [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
- [Cmake tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)

The following plugins are recommended:
- [Cmake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
- [GLSL Lint](https://marketplace.visualstudio.com/items?itemName=CADENAS.vscode-glsllint)
- [Shader languages support](https://marketplace.visualstudio.com/items?itemName=slevesque.shader)
- [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
- [Git lens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)  

#### Build using cmake
You can use cmake to generate the build files to compile the project (check the required cmake version in the CMakeLists.txt in the root directory).  
Assuming you are using git bash or powershell follow this commands. You can also use the cmake vscode plugin and skip this part.
1. Create build file in the root of the project and go inside  
    ```
    mkdir build
    cd build
    ```
2. Configure the cmake files (you can also use the CMake GUI)  
    ``cmake ..``
3. Build your project. You can use the default compiler assigned by cmake or use other you like.  
    ``cmake --build .``  

Alternatively you could use also the cmake GUI.  
#### Launch and configure editor.
You should get an `.exe` file with the aplication as a result. If using vscode you should be able to launch it with the `launch.json` (pressing f5).

- If you want the `launch.json` in vscode you can copy an rename the file `.vscode\others\launch_example.jsonc` to `.vscode\launch.json`.  
- If you want the `settings.json` in vscode you can copy an rename the file `.vscode\others\settings_example.jsonc` to `.vscode\settings.json`.
### Format
Clang format is used to mantain consistency, the format file is in the root of the project (`.clang-format`). If using vscode you can apply it on save by adding `"editor.formatOnSave": true,` to the  `.vscode\settings.json` file (clang-format already comes with the c++ plugin).

If you want to apply clang format to several files recursively you can use from this [project](https://github.com/eklitzke/clang-format-all) the bash script, if using vscode in windows you will problably need to set the `clang-format.exe` path in the script.
In my case in windows it was located at `%USERPROFILE%\.vscode\extensions\ms-vscode.cpptools-1.6.0\LLVM\bin\clang-format.exe`
### Linter
If you want to receive `clang-tidy` warnings about typical programming errors you can install clang-tidy and then install the [Clangd](https://marketplace.visualstudio.com/items?itemName=llvm-vs-code-extensions.vscode-clangd) plugin in vscode. 

The configuration file is in the root of the project so it should be detected directly by the vscode extension

It will conflict with intellisense so you should deactivate it. (`in settings.json` add `"C_Cpp.intelliSenseEngine": "Disabled",`) 

### Profiler
!["Example tracy profiler GUI"](others/exampleTracyProfiler.jpg)

In case you want to profile the code, the project supports [Tracy](https://github.com/wolfpld/tracy/tree/v0.10?tab=readme-ov-file). It can be activated or deactivated with the flag `-DTRACY_ENABLE`. In vscode you can add the option `.vscode/settings.json`. You can see an example in `others/.vscode/.settings_example.jsonc`. 
E.g:
```
    "cmake.configureArgs": [
        "-DTRACY_ENABLE=ON"
    ],
```
The Tracy library is a submodule of this project. To get the GUI you can download the binary files from the Tracy repository.
https://github.com/wolfpld/tracy/releases/tag/v0.10.

Resources:
 - In case you need more information about Tracy check the [documentation](https://github.com/wolfpld/tracy/releases/latest/download/tracy.pdf) from the repository. E.g: In case you need to compile the GUI check section 2.3.
 - [Here](https://luxeengine.com/integrating-tracy-profiler-in-cpp/) they explain how to integrate Tracy into a project.
 - [Here](https://www.youtube.com/watch?v=ghXk3Bk5F2U) is a video explaning some of the Tracy features 
## Authors
Pablo Sánchez-Herrero Gómez