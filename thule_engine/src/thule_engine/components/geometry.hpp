#pragma once
#include <vector>

#include "thule_engine/geom/geomDef.hpp"
class Geometry
{
   public:
    std::vector<Vertex> vertices;
    std::vector<face_t> faces;
};