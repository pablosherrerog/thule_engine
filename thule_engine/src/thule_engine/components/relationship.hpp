#pragma once
// extracted from https://skypjack.github.io/2019-06-25-ecs-baf-part-4/
#include <cstdlib>

#include "thule_engine/core/ecs/typesECS.hpp"
//#include <thule_engine/core/ecs/coordinator.hpp>

struct Relationship
{
    std::size_t num_children{0};
    Entity first_child{NULL_ENTITY};
    Entity last_child{NULL_ENTITY};
    Entity prev_sibling{NULL_ENTITY};
    Entity next_sibling{NULL_ENTITY};
    Entity getParent() const { return m_parent; }
    // Only coordinator should call setParent (because it updates root entities). Review if
    // beter alternative.
    void setParent(Entity a_parent) { m_parent = a_parent; }

   private:
    Entity m_parent{NULL_ENTITY};
};