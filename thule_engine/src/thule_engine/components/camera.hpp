#pragma once
#include "thule_engine/geom/geomDef.hpp"
class Camera
{
   public:
    // TODO(Pablo): the buttons variables shouldn't be in the Camera component
    float zoomSpeed = 0.08F;
    Camera() : view(mat4_t(1.0F)), projection(mat4_t(1.0F)), targetPoint(vec3_t(1.0F)) {}
    float zoom = 1.0F;
    inline static const float k_defaultSpeed = 2.5F;
    float speed = k_defaultSpeed;
    vec3_t targetPoint;
    mat4_t view;
    mat4_t projection;
    bool orbitMode = false;
    bool orbitModeButtonPressed = false;
    bool resetModeButtonPressed = false;
    float pitch = -1;  // in degrees
    float yaw = -1;    // in degrees
    float rotationSensitivity = 0.2F;
    float lastXMousePos = -1;
    float lastYMousePos = -1;
    bool lastMousePosInitialized = false;
    float fov = 45.0F;
    float aspectRatio = -1;
    float nearDistance = 0.1F;
    float farDistance = 100.0F;
    bool enabledCameraInput = false;
};