#pragma once
#include "thule_engine/geom/geomDef.hpp"
class SpotLight
{
   public:
    // We will use the position/direction from the transform

    color3_t ambient;
    color3_t diffuse;
    color3_t specular;

    float linear = 0.07F;
    float quadratic = 0.017F;

    float cutOff = 0.97629F;      // in radians (cos 12.5 degrees)
    float outerCutoff = 0.9537F;  // in radians (cos 17.5 degrees)
    SpotLight()
        : ambient{color3_t(0.0, 0.0, 0.0)},
          diffuse{color3_t(0.0, 0.0, 0.0)},
          specular{color3_t(0.0, 0.0, 0.0)} {};
};