#include "thule_engine/common/utils.hpp"

//#include "thule_engine/core/engine.h"

#include <fstream>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <unordered_map>

#include "thule_engine/common/log.hpp"
#include "thule_engine/components/transform.hpp"
#include "thule_engine/geom/geomDef.hpp"

void Utils::printErrorAndExit(std::string a_errorMessage)
{
    // for the moment I'm going to require to press enter before exiting by default to see the error
    // trace
    // std::cerr << a_errorMessage << std::endl;
    // CommonSettings::instance().getMainCore()->exitEngine(FAILURE);
    printErrorAndWaitExit(a_errorMessage);
}
void Utils::printErrorAndWaitExit(std::string a_errorMessage)
{
    std::cerr << a_errorMessage << std::endl;
    // TODO: Refactor this to free engine resources. (With exit event or similar)
    waitUntilKeyPress();
    // CommonSettings::instance().getEnginePtr()->exitEngine(FAILURE);
}

void Utils::waitUntilKeyPress()
{
    std::cout << "Press Enter to Exit" << std::endl;
    std::cin.ignore();
}

std::string Utils::readtTxtFile(std::string a_inputPath)
{
    if (a_inputPath == "")
    {
        Utils::printErrorAndExit("Error reading txt file, empty input path");
    }
    std::string fileData = "";

    std::ifstream myfile(a_inputPath);
    if (myfile.is_open())
    {
        std::string line;
        while (std::getline(myfile, line))
        {
            fileData += line + "\n";
        }
        myfile.close();
    }
    else
    {
        Utils::printErrorAndExit("Error! Couldn't open the file " + a_inputPath);
    }

    return fileData;
}
std::string Utils::transformToStr(Transform *a_trans)
{
    vec3_t localTrans = a_trans->getLocalPosition();
    return std::to_string(localTrans.x) + ", " + std::to_string(localTrans.x) + ", " +
           std::to_string(localTrans.x);
}

/*void Utils::log(std::string a_trace, int a_logLevel) {
        if (a_logLevel >= logLevel) {
                std::cout << a_trace << std::endl;
        }
}*/

std::string Utils::mat4ToString(mat4_t a_mat4)
{
    // glm orders the matrices in major column order
    /*
    0  4  8  12
    1  5  9  13
    2  6 10  14
    3  7 11  15
    */
    std::string stringMatrix = "";
    mat4_t transposedMat = glm::transpose(a_mat4);
    for (int i = 0; i < transposedMat.length(); i++)
    {
        stringMatrix += glm::to_string(transposedMat[i]) + "\n";
    }
    return stringMatrix;
}

/*void Utils::debug(std::string a_trace)
{
        log("DEBUG: " + a_trace, kLowLogPriority);
}*/

std::string Utils::getFileExt(const std::string &s)
{
    size_t i = s.rfind('.', s.length());
    if (i != std::string::npos)
    {
        return (s.substr(i + 1, s.length() - i));
    }

    return ("");
}

std::string Utils::getStrCplusplusVersion(int a_cplusplusVariable)
{
    // Call it with the __cplusplus  variable
    std::map<int, std::string> versionsMap = {
        {201703L, "C++17"}, {201103L, "C++11"}, {199711L, "C++98"}};
    std::map<int, std::string>::iterator itr;
    for (itr = versionsMap.begin(); itr != versionsMap.end(); ++itr)
    {
        if (a_cplusplusVariable == itr->first)
        {
            return itr->second;
        }
    }
    return "Unrecognized C++ version";
}