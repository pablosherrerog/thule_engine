#include "log.hpp"

int Log::logLevel = Log::logType::k_normal;

std::vector<int> Log::activeChannels = {Log::channelType::ALL};

bool Log::isChannelActive(int a_channel)
{
    for (int activeChannel : activeChannels)
    {
        if (a_channel == activeChannel)
        {
            return true;
        }
        if (activeChannel == channelType::ALL)
        {  // without filter
            return true;
        }
    }
    return false;
}

void Log::log(const std::string& a_trace, int a_logLevel, int a_channel)
{
    if (a_logLevel >= logLevel && isChannelActive(a_channel))
    {
        std::cout << a_trace << std::endl;
    }
}
void Log::error(const std::string& a_trace) { log("ERROR: " + a_trace, logType::k_error); }
void Log::warning(const std::string& a_trace) { log("WARNING: " + a_trace, logType::k_warning); }
void Log::info(const std::string& a_trace) { log("INFO: " + a_trace, logType::k_normal); }
void Log::debug(const std::string& a_trace) { log("DEBUG: " + a_trace, logType::k_debug); }
void Log::debugLow(const std::string& a_trace)
{
    log("DEBUG LOW: " + a_trace, logType::k_debugLow);
}