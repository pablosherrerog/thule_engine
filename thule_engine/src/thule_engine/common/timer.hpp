#include <chrono>
#include <locale>
#include <string>

// Timer for benchmarking from The Cherno video https://www.youtube.com/watch?v=YG4jexlSAjc. It will
// start when the object is created and will stop when it's out of scope.
class Timer
{
   public:
    Timer();
    ~Timer();
    // Stops the counters and returns the milliseconds ellapsed since the object was created.
    double getMilliseconds() const;
    void printInfo() const;
    void printInfo(const std::string &a_extraInfo) const;

   private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_startTimePoint;
};