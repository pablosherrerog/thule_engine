#pragma once
#include <cassert>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thule_engine/common/log.hpp>

// https://stackoverflow.com/questions/348833/how-to-know-the-exact-line-of-code-where-an-exception-has-been-caused
class MyException : public std::runtime_error
{
    std::string m_msg;

   public:
    MyException(const std::string &a_arg, const char *a_file, int a_line)
        : std::runtime_error(a_arg)
    {
        std::ostringstream o;
        o << a_file << ":" << a_line << ": " << a_arg;
        m_msg = o.str();
    }
    ~MyException() noexcept override = default;
    const char *what() const noexcept override { return m_msg.c_str(); }
};

// THULE_EXCEPTION is for runtime user errors, THULE_ASSERTION_ERROR is for developer errors (things
// that should not happen).
#define THULE_EXCEPTION(errorMsg) throw MyException(errorMsg, __FILE__, __LINE__);
#define THULE_ASSERTION_ERROR(errorMsg) \
    Log::error(errorMsg);               \
    assert(false);
