#pragma once
#include <iostream>
#include <vector>
class Log
{
   public:
    enum logType
    {
        k_debugLow,
        k_debug,
        k_normal,
        k_warning,
        k_error,
    };
    enum channelType
    {
        ALL,
        GENERAL,
        CORE
    };
    static std::vector<int> activeChannels;
    static int logLevel;
    static void log(const std::string& a_trace, int a_logLevel,
                    int a_channel = channelType::GENERAL);
    static void error(const std::string& a_trace);
    static void warning(const std::string& a_trace);
    static void info(const std::string& a_trace);
    static void debug(const std::string& a_trace);
    static void debugLow(const std::string& a_trace);
    static bool isChannelActive(int a_channel);
};