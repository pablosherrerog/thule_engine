#pragma once
#include <string>
namespace common
{
class Definitions
{
   public:
    // enum logPriority;
    static std::string m_mainFolderPath;
    static std::string m_engine_path;
    static std::string m_sourcePath;
    static std::string m_shadersFolderPath;
    static std::string m_resourcesFolderPath;
    static std::string m_texturesFolderPath;
    static std::string m_defaultMeshesPath;

    static void initializeDefinitions();

   private:
    const std::string& localMainPathFromExe;
};
}  // namespace common