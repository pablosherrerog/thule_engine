#pragma once
#include <string>
#include <thule_engine/core/ecs/typesECS.hpp>
#include <unordered_map>

#include "thule_engine/geom/geomDef.hpp"

struct Transform;
class Utils
{
   public:
    constexpr static const int kLowLogPriority = 1;
    constexpr static const int kMediumLogPriority = 2;
    constexpr static const int kHighLogPriority = 3;

    static const int FAILURE = 1;
    static const int SUCESS = 0;

    static const int logLevel = 0;
    static std::string getExePath();
    // lower priority logs will not be shown
    static void printErrorAndExit(std::string a_errorMessage);
    static void printErrorAndWaitExit(std::string a_errorMessage);
    static void waitUntilKeyPress();
    static std::string readtTxtFile(std::string a_inputPath);

    static void log(std::string a_trace, int logLevel = kMediumLogPriority);

    static std::string mat4ToString(mat4_t a_mat4);
    static std::string transformToStr(Transform* a_trans);

    static void debug(std::string a_trace);
    static std::string getStrCplusplusVersion(int a_cplusplusVariable);
    static std::string getFileExt(const std::string& s);
};