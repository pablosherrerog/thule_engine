#include "maths.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>  // after <glm/glm.hpp>

#include "glm/gtc/quaternion.hpp"

mat4_t Maths::transformationMatrixCalculation(const vec3_t& a_trans, const quat_t& a_rotation,
                                              const vec3_t& a_scale)
{
    // glm matrices use column wise order.
    mat4_t translationMatrix = glm::translate(mat4_t(1.0F), a_trans);
    mat4_t rotationMatrix = glm::mat4_cast(a_rotation);
    mat4_t scaleMatrix = glm::scale(a_scale);
    return translationMatrix * rotationMatrix * scaleMatrix;
}

// Input euler angles in degrees.
quat_t Maths::eulerDegToQuaternion(const vec3_t& a_eulerAnglesDeg)
{
    glm::quat quatAroundX =
        glm::angleAxis(glm::radians(a_eulerAnglesDeg.x), glm::vec3(1.0, 0.0, 0.0));
    glm::quat quatAroundY =
        glm::angleAxis(glm::radians(a_eulerAnglesDeg.y), glm::vec3(0.0, 1.0, 0.0));
    glm::quat quatAroundZ =
        glm::angleAxis(glm::radians(a_eulerAnglesDeg.z), glm::vec3(0.0, 0.0, 1.0));
    // XYZ rotation order (remember that the order of aplication is inverse to the multiplication
    // order).
    return quatAroundZ * quatAroundY * quatAroundX;
}
vec3_t Maths::quaternionToEuler(const quat_t& a_quaternion)
{
    return glm::degrees(glm::eulerAngles(a_quaternion));
}