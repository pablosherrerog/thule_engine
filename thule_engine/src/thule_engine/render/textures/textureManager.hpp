#pragma once
#include <memory>
#include <thule_engine/render/textures/texture.hpp>
class TextureManager
{
   public:
    static std::string textureTypeToStr(TextureType a_textureType);
    static std::shared_ptr<Texture> create(const std::string& a_path, TextureType a_textureType);
};