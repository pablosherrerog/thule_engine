#pragma once
// From cherno
// https://www.youtube.com/watch?v=93bavRgVcwA&list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT&index=72
#include <stdint.h>

#include <memory>

struct FramebufferSpecification
{
    uint32_t width = 0;
    uint32_t height = 0;
};
class Framebuffer
{
   public:
    virtual const FramebufferSpecification& getSpecification() const = 0;
    // If we didn't have this virtual destructor to be overwritten for the one in openGLFramebuffer
    // and we deleted a Framebuffer pointer (that is a openGLFramebuffer) the openGLFramebuffer
    // destructor would never be called, only the default Framebuffer destructor
    // https://youtu.be/ETIhjdVBH-8?t=294

    virtual ~Framebuffer() = default;
    static std::shared_ptr<Framebuffer> create(const FramebufferSpecification& a_specification);
    virtual void updateFromSpecification() = 0;
    virtual void resize(uint32_t a_width, uint32_t a_height) = 0;
    virtual uint32_t getRendererID() const = 0;
    virtual uint32_t getTextureColorID() const = 0;
};