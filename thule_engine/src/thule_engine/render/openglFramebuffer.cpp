#include <glad/glad.h>

#include <memory>
#include <string>
#include <thule_engine/common/exceptionUtils.hpp>
#include <thule_engine/common/log.hpp>
#include <thule_engine/render/openglFramebuffer.hpp>

OpenGLFramebuffer::OpenGLFramebuffer(const FramebufferSpecification& a_specification)
{
    /* https://stackoverflow.com/questions/2213030/whats-the-concept-of-and-differences-between-framebuffer-and-renderbuffer-in-op
    The framebuffer is not a buffer but a object containing several attached buffers.
    The buffers can be textures or renderbuffers.
    - Renderbuffers: (if you don't need sampling) contains the raw buffer data (array of bytes, ints
    or pixels). Since they use a native format it's hard to read from them but once they are painted
    you can copy the content to the screen or other renderbuffers really fast (e.g for swapping
    buffers).
    - Texture: (if you need sampling) uses a standard format so it's slower since you need to do
    conversion
    */
    // https://learnopengl.com/Advanced-OpenGL/Framebuffers
    // Create framebuffer.
    glGenFramebuffers(1, &m_rendererID);
    glBindFramebuffer(GL_FRAMEBUFFER, m_rendererID);
    // Create color attachment texture
    glGenTextures(1, &m_textureColorID);
    glBindTexture(GL_TEXTURE_2D, m_textureColorID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, a_specification.width, a_specification.height, 0, GL_RGB,
                 GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Attach the color texture to the framebuffer.
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_textureColorID,
                           0);
    // Create a renderbuffer object for depth and stencil attachment (we won't be sampling these)
    glGenRenderbuffers(1, &m_rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
    // Use a single renderbuffer object for both a depth AND stencil buffer.
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, a_specification.width,
                          a_specification.height);
    // Attach the render buffer.
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_rbo);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        THULE_ASSERTION_ERROR("ERROR::FRAMEBUFFER:: Framebuffer is not complete!");
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void OpenGLFramebuffer::updateFromSpecification()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_rendererID);
    // Resize color texture
    glBindTexture(GL_TEXTURE_2D, m_textureColorID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_specification.width, m_specification.height, 0, GL_RGB,
                 GL_UNSIGNED_BYTE, nullptr);
    glBindTexture(GL_TEXTURE_2D, 0);
    // Resize renderbuffer (with depth and stencil buffer)
    glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
    // Use a single renderbuffer object for both a depth AND stencil buffer.
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_specification.width,
                          m_specification.height);

    // The glViewport wil be used to map the normalized-device coordinates to screen coordinates.
    glViewport(0, 0, m_specification.width, m_specification.height);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void OpenGLFramebuffer::resize(uint32_t a_width, uint32_t a_height)
{
    m_specification.width = a_width;
    m_specification.height = a_height;
    updateFromSpecification();  // Resize the framebuffer attachments.
}
OpenGLFramebuffer::~OpenGLFramebuffer()
{
    glDeleteFramebuffers(1, &m_rendererID);
    glDeleteTextures(1, &m_textureColorID);
}