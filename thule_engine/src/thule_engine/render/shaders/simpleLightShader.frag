#version 330 core

struct Light {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
}; 

out vec4 FragColor;
in vec3 normal;
in vec3 fragPos; 
  
uniform vec3 viewPos;
  
uniform Light light;
uniform Material material;

void main()
{
    // ambient light
    vec3 ambient = light.ambient * material.ambient;
    
    // diffuse light
    // normalize to apply dot product to measure the intensity of the ilumination
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(light.position - fragPos);
    // max in case the angle between norm and lightDir is greater than 90 degrees 
    // (negative dot product), it will set the value from the second parameter 0.0
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = (diff * material.diffuse) * light.diffuse;

    //specular light
    vec3 viewDir = normalize(viewPos - fragPos);
    // reverse lightDir to go from the the light position to the fragment position
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = material.specular *  light.specular * spec;  
    vec3 result = (ambient + diffuse + specular);
    FragColor = vec4(result, 1.0);

}