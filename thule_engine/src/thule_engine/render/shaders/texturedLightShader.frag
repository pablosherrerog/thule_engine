#version 330 core
// TODO: an alternative to this is do use deferred shading and do one pass per light and add them
#define NUM_DIR_LIGHTS 1
#define NUM_POINT_LIGHTS 4
#define NUM_SPOT_LIGHTS 1 
struct DirLight {
    vec3 direction;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

	float linear;
	float quadratic;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float linear;
    float quadratic;
    
    // This angles comes in format like this cos(45) instead of degrees/radians to avoid using the acos to compare (expensive).
	float cutoff;  
    float outerCutoff;

};



struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;  
    sampler2D texture_emissive1;  
    float shininess;
}; 

struct MaterialColors {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
}; 

struct FragmentResultColor {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

out vec4 FragColor;
in vec3 normal;
in vec3 fragPos; 
in vec2 texCoord;
  
uniform vec3 viewPos;
  
uniform DirLight dirLights[NUM_DIR_LIGHTS];
uniform PointLight pointLights[NUM_POINT_LIGHTS];
uniform SpotLight spotLights[NUM_SPOT_LIGHTS];
uniform Material material;
uniform float time;
uniform MaterialColors materialColors;

FragmentResultColor calcCommonIlumination(vec3 a_dirLight, vec3 a_ambient, vec3 a_diffuse, vec3 a_specular, vec3 a_normal, vec3 a_viewDir){
    FragmentResultColor fColor;
    // a_dirLight must be a normalized vector from the fragment to the light
    fColor.ambient = a_ambient * materialColors.ambient * texture(material.texture_diffuse1, texCoord).rgb;
    // max in case the angle between norm and lightDir is greater than 90 degrees 
    // (negative dot product), it will set the value from the second parameter 0.0
    // DIFFUSE
    float diff = max(dot(a_normal, a_dirLight), 0.0);
    fColor.diffuse = diff * a_diffuse * materialColors.diffuse * texture(material.texture_diffuse1, texCoord).rgb;
    // SPECULAR
    // Reflect expect a vector from the light to the fragment (that's why we negate it)
    vec3 reflectDir = reflect(-a_dirLight, a_normal);
    float spec = pow(max(dot(a_viewDir, reflectDir), 0.0), material.shininess);
    fColor.specular =  a_specular * spec *  texture(material.texture_specular1, texCoord).rgb * materialColors.specular;  
    return fColor;
}

FragmentResultColor calculateDistanceAttenuation(vec3 a_lightPos, FragmentResultColor a_fcolor, float a_linear, float a_quadratic){
    // ATTENUATION
    float distance = length(a_lightPos - fragPos);
    float attenuation = 1.0 / (1.0 + a_linear * distance + a_quadratic * (distance * distance));    
    a_fcolor.ambient  *= attenuation;
    a_fcolor.diffuse  *= attenuation;
    a_fcolor.specular *= attenuation;
    return a_fcolor;
}

vec3 calcDirectionalLight(DirLight a_light, vec3 a_normal, vec3 a_viewDir){
    // The direction of the light needed from the fragment to the lightsource 
    // but is expected to come as a global direction (from the light, to another point) 
    // since is easier to define (that why is negated)
    vec3 lightDir = normalize(-a_light.direction);
    FragmentResultColor fcolor = calcCommonIlumination(lightDir, a_light.ambient, a_light.diffuse, a_light.specular, a_normal, a_viewDir);
    return fcolor.ambient + fcolor.diffuse + fcolor.specular;
}

vec3 calcPointLights(PointLight a_light, vec3 a_normal, vec3 a_viewDir){

    vec3 lightDir = normalize(a_light.position - fragPos);
    FragmentResultColor fcolor = calcCommonIlumination(lightDir, a_light.ambient, a_light.diffuse, a_light.specular, a_normal, a_viewDir);
    fcolor = calculateDistanceAttenuation(a_light.position, fcolor, a_light.linear, a_light.quadratic);
    return fcolor.ambient + fcolor.diffuse + fcolor.specular;
}



vec3 calcSpotLights(SpotLight a_light, vec3 a_normal, vec3 a_viewDir){
    vec3 lightDir = normalize(a_light.position - fragPos);
    FragmentResultColor fcolor = calcCommonIlumination(lightDir, a_light.ambient, a_light.diffuse, a_light.specular, a_normal, a_viewDir);
    fcolor = calculateDistanceAttenuation(a_light.position, fcolor, a_light.linear, a_light.quadratic);
    // Regulate instensity of the light depending of the spotlight cone.
    float theta = dot(lightDir, normalize(-a_light.direction));
    float epsilon   = a_light.cutoff - a_light.outerCutoff;
    // We clamp to 0 and 1 since the formula will give values > 1 for angles inside the inner edge and < 0 outside the outer edge.
    float intensity = clamp((theta - a_light.outerCutoff) / epsilon, 0.0, 1.0);
    // Ambient is not affected by the intensity since we want to assure that some of the spotlight ilumination is shown outside the outer edge
    fcolor.diffuse *= intensity;
    fcolor.specular *= intensity;
    return fcolor.ambient + fcolor.diffuse + fcolor.specular;
}

void main()
{
    vec3 norm = normalize(normal);
    vec3 viewDir = normalize(viewPos - fragPos);
    vec3 dirLightsResults = vec3(0, 0 ,0);
    vec3 pointLightsResults = vec3(0, 0 ,0);
    vec3 spotLightsResults = vec3(0, 0 ,0);

    for(int i = 0; i < NUM_DIR_LIGHTS; i++){
        dirLightsResults += calcDirectionalLight(dirLights[i], norm, viewDir);
    }
    for(int i = 0; i < NUM_POINT_LIGHTS; i++){
        pointLightsResults += calcPointLights(pointLights[i], norm, viewDir);
    }
    for(int i = 0; i < NUM_SPOT_LIGHTS; i++){
        spotLightsResults += calcSpotLights(spotLights[i], norm, viewDir);
    }
    // Emission
    vec3 emission = vec3(0, 0, 0);
    //Emission
    //Rough check for blackbox inside spec texture (to avoid drawing the emissive in the metal areas)
    //TODO: branching is not good, create a new shader if needed
    if (texture(material.texture_specular1, texCoord).r == 0.0)  
    {
        //apply emission texture
        emission = texture(material.texture_emissive1, texCoord).rgb;
        
        //some extra fun stuff with "time uniform" 
        emission = texture(material.texture_emissive1, texCoord + vec2(0.0,sin(time))).rgb;   //moving
        emission = emission * (sin(time) * 0.5 + 0.5) * 2.0;                             //fading
    }

    // TODO: not sure about the addition, two 0.5 lights can set an object to white?
    vec3 result = dirLightsResults + pointLightsResults + spotLightsResults + emission;
    //result = spotLightsResults;
    FragColor = vec4(result, 1.0);
	//FragColor = vec4(1.0,0.0,0.0, 1.0);
}   