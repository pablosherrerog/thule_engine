#pragma once
#include <glm/glm.hpp>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

struct Texture;
class Shader
{
   public:
    unsigned int shaderProgram = 9999999;
    unsigned int vertexShader = 9999999;
    unsigned int fragmentShader = 9999999;
    std::vector<Texture*> texturesPtrs;

    Shader(std::string a_pathVertexShader, std::string a_pathFragmentShader);
    ~Shader();
    void useMaterialShader();
    void setBoolUniform(const std::string& name, bool value) const;
    void setIntUniform(const std::string& name, int value) const;
    void setFloatUniform(const std::string& name, float value) const;
    void setMat4(const std::string& name, const glm::mat4& mat) const;

    void setVec3(const std::string& name, const glm::vec3& mat) const;

    void setVec4(const std::string& name, const glm::vec4& a_vec4);
    std::string getVertexShaderPath() const { return m_vertexShaderPath; }
    std::string getFragmentShaderPath() const { return m_fragmentShaderPath; }

   private:
    // mutable since we want to keep the other functions as consts
    mutable std::unordered_map<std::string, int> m_uniformLocationCache;
    unsigned int loadShaderProgram(std::string a_pathVertexShader,
                                   std::string a_pathFragmentShader);
    void checkSuccessShaderCompilation(unsigned int a_shader, std::string a_type);
    void checkSuccessProgramLinking(unsigned int a_program);
    int getUniformLocation(const std::string& a_uniformName) const;
    std::string m_vertexShaderPath, m_fragmentShaderPath;
};
