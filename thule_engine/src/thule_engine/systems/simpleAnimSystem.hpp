#pragma once
#include "thule_engine/core/ecs/coordinator.hpp"
#include "thule_engine/core/ecs/system.hpp"

class SimpleAnimSystem : public System
{
   public:
    void update(float a_dt);
};