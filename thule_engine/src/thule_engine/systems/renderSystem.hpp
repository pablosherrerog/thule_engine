#pragma once
#include <cstddef>
#include <memory>
#include <vector>

#include "thule_engine/core/ecs/system.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/render/framebuffer.hpp"

class Geometry;
class Renderable;
class Framebuffer;
class Shader;
class RenderSystem : public System
{
   public:
    void update();
    void prepareGeom();
    void createDefaultTextureData();
    void updateMainCamera(Entity a_mainCamera);
    void updateDirLights(const std::vector<Entity> &a_dirlights);
    void updatePointLights(const std::vector<Entity> &a_pointLights);
    void updateSpotLights(const std::vector<Entity> &a_spotlights);
    void initializeRenderer();

   private:
    void createVAOs(const Geometry *a_geomComp, Renderable *a_renComp);
    void setLightProperties(Entity a_entity);
    void prepareMaterialProperties(Entity a_entity);
    Entity m_mainCamera = NULL_ENTITY;
    std::vector<Entity> m_dirLights;
    std::vector<Entity> m_pointLights;
    std::vector<Entity> m_spotLights;
    std::shared_ptr<Framebuffer> m_framebufferPtr = nullptr;
    std::shared_ptr<Shader> m_framebufferShaderPtr = nullptr;
    uint32_t m_quadVAO = 12345;
};