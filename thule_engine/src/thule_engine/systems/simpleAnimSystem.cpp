#include "thule_engine/systems/simpleAnimSystem.hpp"

#include <thule_engine/systems/transformationUpdaterSystem.hpp>

#include "glm/geometric.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/components/simpleAnimation.hpp"
#include "thule_engine/components/transform.hpp"
#include "thule_engine/core/ecs/system.hpp"
#include "thule_engine/geom/geomDef.hpp"

extern Coordinator g_coordinator;
bool transformToPos(Transform &a_transform, const vec3_t &a_objectivePos, float a_dt, float a_speed)
{
    // returns true if it has reached the destination
    vec3_t currentPos = a_transform.getLocalPosition();
    float distanceToObjective = glm::length(a_objectivePos - currentPos);
    // We consider the target reached if it's near.
    float nearDistance = 0.1;
    if (distanceToObjective > nearDistance)
    {
        vec3_t direction = glm::normalize(a_objectivePos - currentPos);
        vec3_t translation = direction * a_dt * a_speed;
        a_transform.translate(translation);
        return false;
    }
    return true;
}
void SimpleAnimSystem::update(float a_dt)
{
    PROFILER_SCOPED;
    for (Entity const &entity : mEntities)
    {
        auto &transf = g_coordinator.getComponent<Transform>(entity);
        auto &simpleAnim = g_coordinator.getComponent<SimpleAnimation>(entity);
        transf.rotate(simpleAnim.rotationPerSecond * a_dt);

        // Select target pos
        if (simpleAnim.goingToTargetPos)
        {
            simpleAnim.currentTargetPos = simpleAnim.transformationTargetPos;
        }
        else
        {
            simpleAnim.currentTargetPos = simpleAnim.transformationStartingPos;
        }
        bool reachedGoal = transformToPos(transf, simpleAnim.currentTargetPos, a_dt,
                                          simpleAnim.transformationSpeed);
        // Change target goal if needed.
        if (reachedGoal && simpleAnim.loopTranslationAnimation)
        {
            simpleAnim.goingToTargetPos = !simpleAnim.goingToTargetPos;
        }
    }
}
