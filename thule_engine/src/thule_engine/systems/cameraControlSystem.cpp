#include "cameraControlSystem.hpp"

#include <GLFW/glfw3.h>

#include <functional>  // std::bind
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>

#include "glm/gtx/string_cast.hpp"
#include "thule_engine/common/log.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/components/camera.hpp"
#include "thule_engine/components/transform.hpp"
#include "thule_engine/core/application.hpp"
#include "thule_engine/core/ecs/coordinator.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/core/events/appEvents.hpp"
#include "thule_engine/core/events/engineEvents.hpp"
#include "thule_engine/core/events/event.hpp"
#include "thule_engine/core/events/mouseEvents.hpp"
#include "thule_engine/core/input/input.hpp"
#include "thule_engine/core/input/keyCodes.hpp"
#include "thule_engine/geom/geomDef.hpp"

extern Coordinator g_coordinator;

void mouseButtonCallback(GLFWwindow *a_window, int a_button, int a_action, int a_mods)
{
    if (a_button == GLFW_MOUSE_BUTTON_RIGHT && a_action == GLFW_PRESS)
    {
        glm::vec2 mousePos = Input::getMousePosition();
        // Get current event, if any, to remove it
        Dispatcher::getPostedEvent<app_events::MouseDisplacementWindowEvent>(true);
        // Post a new one with the displacment to 0
        Dispatcher::post(app_events::MouseDisplacementWindowEvent(mousePos));
    }
}
void CameraControlSystem::init()
{
    std::queue<app_events::WindowResizeEvent> initialWindowSizeEvents =
        Dispatcher::getPostedEvent<app_events::WindowResizeEvent>();
    if (initialWindowSizeEvents.size() != 1)
    {
        Utils::printErrorAndExit(
            "Error! there should be an event from the windows class indicating the initial window "
            "dimensions");
    }
    // The last one is the last one updated.
    onSceneViewportResize(initialWindowSizeEvents.back());
    for (Entity const &entity : mEntities)
    {
        resetCameraRotation(entity);
        // To create a function pointer to a member function we need to pass as the first argument
        // of std::bind the reference to the member function. The placeholder will represent the one
        // function argument.
        Dispatcher::subscribe(
            event_types::EventType::k_sceneViewportResize,
            std::bind(&CameraControlSystem::onSceneViewportResize, this, std::placeholders::_1));

        Dispatcher::subscribe(
            event_types::EventType::k_mouseScroll,
            std::bind(&CameraControlSystem::onScroll, this, std::placeholders::_1));
    }
    // TODO(Pablo): this can be probably wrapped elsewhere to avoid calling directly glfw.
    Window *window = &Application::getApp().getWindow();
    glfwSetMouseButtonCallback(static_cast<GLFWwindow *>(window->getNativeWindow()),
                               mouseButtonCallback);
}

void CameraControlSystem::update(float a_dt)
{
    PROFILER_SCOPED;
    int numCameras = 0;
    for (Entity const &entity : mEntities)
    {
        if (numCameras > 1)
        {
            // TODO: allow several cameras or lock it to one
            Utils::printErrorAndExit("Error! the system is no prepared to allow several cameras");
        }
        auto &trans = g_coordinator.getComponent<Transform>(entity);
        auto &camera = g_coordinator.getComponent<Camera>(entity);
        if (camera.enabledCameraInput)
        {
            processInput(&trans, &camera, a_dt);
        }

        // calculate view matrix after moving the camera
        calculateViewMatrix(&trans, &camera);

        recalculateCameraProjection(&camera);
        numCameras += 1;
    }
}

void CameraControlSystem::recalculateCameraProjection(Camera *const a_cameraPtr)
{
    a_cameraPtr->projection = glm::perspective(glm::radians(a_cameraPtr->fov * a_cameraPtr->zoom),
                                               a_cameraPtr->aspectRatio, a_cameraPtr->nearDistance,
                                               a_cameraPtr->farDistance);
}

void CameraControlSystem::onSceneViewportResize(const Event &a_event)
{
    // When the main scene viewport changes we need to update the aspect ratio of each camera.
    // Therefore we will avoid getting streched or compressed images in the viewport.
    auto windowResizeEvent = static_cast<const app_events::WindowResizeEvent &>(a_event);
    for (Entity const &entity : mEntities)
    {
        auto &camera = g_coordinator.getComponent<Camera>(entity);
        // Set a limit on how much can the height be modified to avoid 0 division.
        if (windowResizeEvent.getHeight() < 0.1)
        {
            camera.aspectRatio = static_cast<float>(windowResizeEvent.getWidth()) / 0.1F;
        }
        else
        {
            camera.aspectRatio = static_cast<float>(windowResizeEvent.getWidth()) /
                                 static_cast<float>(windowResizeEvent.getHeight());
        }
    }
}

void CameraControlSystem::onScroll(const Event &a_event)
{
    auto scrollEvent = static_cast<const MouseEvents::ScrollEvent &>(a_event);
    for (Entity const &entity : mEntities)
    {
        auto &camera = g_coordinator.getComponent<Camera>(entity);
        if (camera.enabledCameraInput)
        {
            camera.zoom -= scrollEvent.getOffsetY() * camera.zoomSpeed;
            // Clamp values.
            if (camera.zoom < 0.1F)
            {
                camera.zoom = 0.1F;
            }
            else if (camera.zoom > 1.0F)
            {
                camera.zoom = 1.0F;
            }
        }
    }
}

void CameraControlSystem::resetCameraRotation(Entity a_cameraEnt)
{
    resetCameraRotation(&g_coordinator.getComponent<Camera>(a_cameraEnt));
}
void CameraControlSystem::resetCameraRotation(Camera *a_cameraPtr)
{
    a_cameraPtr->yaw = -90;  // to point to negative z axis
    a_cameraPtr->pitch = 0;
}

void CameraControlSystem::calculateViewMatrix(Transform *a_trans, Camera *a_camera)
{
    vec3_t target = a_camera->targetPoint;
    glm::mat4 view = glm::lookAt(a_trans->getLocalPosition(), target, vec3_t(0, 1, 0));
    a_camera->view = view;
}

void CameraControlSystem::processInput(Transform *a_trans, Camera *a_camera, float a_dt)
{
    // multiply the movement by delta time so if you have less or more frames the movement will
    // be the same
    bool currentOrbitMode = a_camera->orbitMode;
    if (Input::isKeyPressed(Key::R) && a_camera->resetModeButtonPressed == false)
    {
        Log::debug("Camera reset");
        resetCameraRotation(a_camera);
        a_camera->resetModeButtonPressed = true;
    }
    else if (Input::isKeyReleased(Key::R))
    {
        a_camera->resetModeButtonPressed = false;
    }

    if (Input::isKeyPressed(Key::O) && a_camera->orbitModeButtonPressed == false)
    {
        currentOrbitMode = !currentOrbitMode;
        Log::debug("Changed orbit mode to " + std::to_string(currentOrbitMode));
        a_camera->orbitMode = currentOrbitMode;
        a_camera->orbitModeButtonPressed = true;
    }
    else if (Input::isKeyReleased(Key::O) && a_camera->orbitModeButtonPressed == true)
    {
        Log::debug("Released o key");
        a_camera->orbitModeButtonPressed = false;

        resetCameraRotation(a_camera);  // TODO: temporal fix
    }

    if (currentOrbitMode)
    {
        orbitAroundPoint(a_trans, a_camera, vec3_t(0, 0, 0), a_dt);
    }
    else
    {
        // speed is a scalar velocitity is a direction with a magnitude (speed)
        // mru formula x = x0 + v*t

        vec3_t velocity = vec3_t(0, 0, 0);
        if (Input::isKeyPressed(Key::LeftShift))
        {
            a_camera->speed = Camera::k_defaultSpeed * 4;
        }
        if (Input::isKeyReleased(Key::LeftShift))
        {
            a_camera->speed = Camera::k_defaultSpeed;
        }
        if (Input::isKeyPressed(Key::W))
        {
            velocity += a_trans->forward * a_camera->speed;
        }
        if (Input::isKeyPressed(Key::S))
        {
            velocity -= a_trans->forward * a_camera->speed;
        }
        if (Input::isKeyPressed(Key::D))
        {
            velocity += a_trans->right * a_camera->speed;
        }
        if (Input::isKeyPressed(Key::A))
        {
            velocity -= a_trans->right * a_camera->speed;
        }

        a_trans->translate(velocity * a_dt);
        // Log::debug("Local camera pos: " + Utils::transformToStr(a_trans));
        // calculate fps camara rotation
        glm::vec2 mousePos = Input::getMousePosition();
        // Get event that contains the displacement information. Use it to update the camera
        // rotation and then replace the event with the updated information for future usage.
        auto events = Dispatcher::getPostedEvent<app_events::MouseDisplacementWindowEvent>();
        if (events.empty())
        {
            Dispatcher::post(app_events::MouseDisplacementWindowEvent(mousePos));
            events = Dispatcher::getPostedEvent<app_events::MouseDisplacementWindowEvent>();
        }
        auto mouseDisplacementEvent = events.back();

        vec2_t mouseDisplacement = mouseDisplacementEvent.getTotalDisplacement();
        Dispatcher::post(app_events::MouseDisplacementWindowEvent(mousePos));

        // Calculate camera rotation
        float pitch = a_camera->pitch;  // in degrees
        float yaw = a_camera->yaw;

        yaw += mouseDisplacement.x * a_camera->rotationSensitivity;
        pitch += -mouseDisplacement.y * a_camera->rotationSensitivity;

        // to avoid LookAt flip
        if (pitch > 89.0F)
        {
            pitch = 89.0F;
        }
        else if (pitch < -89.0F)
        {
            pitch = -89.0F;
        }
        // we are adding yaw and pitch as degrees
        a_camera->yaw = yaw;
        a_camera->pitch = pitch;

        // recalculate forward with rotation
        // check this image to understand the operations
        // https://uploads.disquscdn.com/images/58fa1f1a3dd8d736a9345b3b168dd55caf0f14d485e9dae7e06b8e185348a42a.png
        // assuming that r is 1
        a_trans->forward.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
        a_trans->forward.y = sin(glm::radians(pitch));
        a_trans->forward.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
        a_trans->setLocalRotation(a_trans->forward);

        // this works because there is no roll
        a_trans->right = glm::normalize(glm::cross(a_trans->forward, vec3_t(0, 1, 0)));

        // update the target with the new position
        vec3_t target = a_trans->getLocalPosition() + a_trans->forward;
        a_camera->targetPoint = target;
    }
}
void CameraControlSystem::orbitAroundPoint(Transform *a_trans, Camera *a_camera, vec3_t a_center,
                                           float a_dt)
{
    // apply soh cah toa here to calculate the x and y values around the circle (orbit)
    // TODO: not implemented
    // TODO: implement arc ball camera, and modify yaw and pitch to mix it with fps camera
    float orbitSpeed = 1.0F;
    float incrementRadians = 1.0F;
    if (Input::isKeyPressed(Key::D))
    {
        float angleIncrement = constrainAngle(incrementRadians * orbitSpeed * a_dt);
        orbitRadians += angleIncrement;
        // Log::debug("Orbit radians: " + std::to_string(orbitRadians) + " \n");
    }
    if (Input::isKeyPressed(Key::A))
    {
        float angleIncrement = constrainAngle(incrementRadians * orbitSpeed * a_dt);
        orbitRadians -= angleIncrement;
    }
    // float radius = glm::length(a_center - a_trans->getLocalPosition());
    float radius = 6.0f;
    a_trans->setLocalPosition(vec3_t(0, 0, radius * cos(orbitRadians)));
    a_trans->setLocalPosition(vec3_t(radius * sin(orbitRadians), 0, 0));
    a_camera->targetPoint = a_center;
}
float CameraControlSystem::constrainAngle(float a_x)
{
    float x = fmod(a_x, 360);
    if (x < 0)
    {
        x += 360;
    }
    return x;
}
