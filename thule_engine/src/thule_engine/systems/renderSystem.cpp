#include "renderSystem.hpp"

#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <stdint.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <memory>
#include <string>
#include <thule_engine/common/pathsDefinitions.hpp>
#include <thule_engine/core/events/appEvents.hpp>
#include <thule_engine/core/events/engineEvents.hpp>
#include <thule_engine/render/framebuffer.hpp>
#include <thule_engine/render/textures/textureManager.hpp>

#include "thule_engine/common/exceptionUtils.hpp"
#include "thule_engine/common/log.hpp"
#include "thule_engine/common/maths.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/components/camera.hpp"
#include "thule_engine/components/directionalLight.hpp"
#include "thule_engine/components/geometry.hpp"
#include "thule_engine/components/material.hpp"
#include "thule_engine/components/pointLight.hpp"
#include "thule_engine/components/renderable.hpp"
#include "thule_engine/components/spotLight.hpp"
#include "thule_engine/components/transform.hpp"
#include "thule_engine/core/ecs/coordinator.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/geom/assimp_loader/assimpLoader.hpp"  // TODO: temporal will be removed eventually
#include "thule_engine/geom/geomDef.hpp"
#include "thule_engine/render/textures/openglTexture.hpp"
#include "thule_engine/render/textures/texture.hpp"

extern Coordinator g_coordinator;

void RenderSystem::initializeRenderer()
{
    FramebufferSpecification framebufferSpecs;
    framebufferSpecs.width = 1280;
    framebufferSpecs.height = 720;

    m_framebufferPtr = Framebuffer::create(framebufferSpecs);
    m_framebufferShaderPtr =
        std::make_shared<Shader>(common::Definitions::m_shadersFolderPath + "/framebuffer.vert",
                                 common::Definitions::m_shadersFolderPath + "/framebuffer.frag");

    float quadVertices[] = {
        // vertex attributes for a quad that fills the entire screen in Normalized Device
        // Coordinates.
        // positions   // texCoords
        -1.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 1.0f, 0.0f,

        -1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  -1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  1.0f, 1.0f};
    // screen quad VAO

    unsigned int quadVBO;
    glGenVertexArrays(1, &m_quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(m_quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)(2 * sizeof(float)));
    Dispatcher::post(engine_events::FramebufferInitialization(m_framebufferPtr));
}
void RenderSystem::updateMainCamera(Entity a_mainCamera) { m_mainCamera = a_mainCamera; }

void RenderSystem::updateDirLights(const std::vector<Entity> &a_dirlights)
{
    m_dirLights = a_dirlights;
}
void RenderSystem::updatePointLights(const std::vector<Entity> &a_pointLights)
{
    m_pointLights = a_pointLights;
}

void RenderSystem::updateSpotLights(const std::vector<Entity> &a_spotlights)
{
    m_spotLights = a_spotlights;
}
void RenderSystem::update()
{
    PROFILER_SCOPED;
    auto &cameraComponent = g_coordinator.getComponent<Camera>(m_mainCamera);
    // Bind to framebuffer and draw scene as we normally would. The data will go into the texture
    glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferPtr->getRendererID());
    glEnable(GL_DEPTH_TEST);  // enable depth testing (is disabled for rendering screen-space quad)

    // make sure we clear the framebuffer's content
    glClearColor(0.0F, 0.4F, 0.0F, 1.0F);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (Entity const &entity : mEntities)
    {
        Transform transf = g_coordinator.getComponent<Transform>(entity);
        Geometry geom = g_coordinator.getComponent<Geometry>(entity);
        Renderable ren = g_coordinator.getComponent<Renderable>(entity);
        auto &mat = g_coordinator.getComponent<Material>(entity);

        if (mat.shader == nullptr)
        {
            // If the material doesn't have a shader we can't render
            continue;
        }
        mat.shader->useMaterialShader();
        prepareMaterialProperties(entity);

        for (unsigned int i = 0; i < mat.textures.size(); i++)
        {
            // active proper texture unit before binding
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, mat.textures[i]->getTextureID());
        }

        // set time in to create cool effects
        mat.shader->setFloatUniform("time", static_cast<float>(glfwGetTime()));

        glm::mat4 model = transf.globalTransf;
        // Utils::debug(g_coordinator.getComponent<Metadata>(entity).name + "\n" +
        // Utils::mat4ToString(model)); model = glm::rotate(model, glm::radians(-55.0f),
        // glm::vec3(1.0f, 0.0f, 0.0f));

        mat.shader->setMat4("model", model);
        mat.shader->setMat4("view", cameraComponent.view);
        mat.shader->setMat4("projection", cameraComponent.projection);
        // TODO(Pablo): most of the frame time is consumed by setLightProperties,  change it.
        setLightProperties(entity);
        glBindVertexArray(ren.VAO);
        int numIndices = geom.faces.size() * 3;  // each face has 3 vertices
        // An error here could mean a problem binding the data to the shader (check if the VAO was
        // initialized).
        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);

        // For debugging is a good practice to set everything back to defaults after rendering

        // https://stackoverflow.com/questions/43377182/opengl-practice-of-binding-to-default-texture
        for (unsigned int i = 0; i < mat.textures.size(); i++)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // Clear default framebuffer (if we have something docked in the main windows it's needed)
    glClearColor(1.0F, 1.0F, 1.0F, 1.0F);
    glClear(GL_COLOR_BUFFER_BIT);
    const bool drawInMainWindow = false;
    if (drawInMainWindow)
    {
        // In the default framebuffer draw a quad plane with the framebuffer color texture.
        // Disable depth test so screen-space quad isn't discarded due to depth test.
        glDisable(GL_DEPTH_TEST);
        m_framebufferShaderPtr->useMaterialShader();
        glBindVertexArray(m_quadVAO);
        glBindTexture(GL_TEXTURE_2D,
                      m_framebufferPtr->getTextureColorID());  // use the color attachment texture
                                                               // as the texture of the quad plane
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
}

void RenderSystem::prepareMaterialProperties(Entity a_entity)
{
    PROFILER_SCOPED;
    auto &mat = g_coordinator.getComponent<Material>(a_entity);
    // activate the shader before setting uniforms
    // TODO: this shader change is expensive?
    // mat.shader->useMaterialShader();
    // set uniforms
    mat.shader->setVec3("materialColors.ambient", mat.ambient);
    mat.shader->setVec3("materialColors.diffuse", mat.diffuse);
    mat.shader->setVec3("materialColors.specular", mat.specular);
    mat.shader->setFloatUniform("material.shininess", mat.shininess);
    // to textures
    unsigned int diffuseNr = 1;
    unsigned int specularNr = 1;
    unsigned int normalNr = 1;
    unsigned int heightNr = 1;
    unsigned int emissiveNr = 1;
    for (unsigned int i = 0; i < mat.textures.size(); i++)
    {
        // retrieve texture number (the N in diffuse_textureN)
        std::string number;
        TextureType textureType = mat.textures[i]->getTextureType();
        std::string textureName = TextureManager::textureTypeToStr(textureType);
        switch (textureType)
        {
            case TextureType::diffuse:
                number = std::to_string(diffuseNr++);
                break;
            case TextureType::specular:
                number = std::to_string(
                    specularNr++);  // transfer unsigned int to stream
                                    // TODO: understand why the specular texture is being replaced
                                    // by the diffuse texture if is not correctly
                                    // TODO: loaded (e.g change names to avoid the shader loading
                                    // the texture and it will use the diffuse texture even after
                                    // unbinding) name = "material.gaergergaergaerg"; number = "";
                break;
            case TextureType::normal:
                number = std::to_string(normalNr++);  // transfer unsigned int to stream
                break;
            case TextureType::height:
                number = std::to_string(heightNr++);  // transfer unsigned int to stream
                break;
            case TextureType::emissive:
                number = std::to_string(emissiveNr++);  // transfer unsigned int to stream
                break;
            default:
                THULE_ASSERTION_ERROR("Error! texture type " +
                                      TextureManager::textureTypeToStr(textureType) +
                                      " was not contempled");
                break;
        }

        mat.shader->setIntUniform(("material." + textureName + number), i);
    }
}
void RenderSystem::setLightProperties(Entity a_entity)
{
    PROFILER_SCOPED;
    // We don't want to call the copy constructor of the material so we use the reference
    auto &mat = g_coordinator.getComponent<Material>(a_entity);
    int i = 0;
    for (const Entity &dirLightEnt : m_dirLights)
    {
        auto &lightTransform = g_coordinator.getComponent<Transform>(dirLightEnt);
        auto &dirComponent = g_coordinator.getComponent<DirectionalLight>(dirLightEnt);
        std::string headerUniform{"dirLights[" + std::to_string(i) + "]."};
        // Should use global rotation
        mat.shader->setVec3(headerUniform + "direction", lightTransform.getLocalRotationEuler());
        mat.shader->setVec3(headerUniform + "ambient", dirComponent.ambient);
        mat.shader->setVec3(headerUniform + "diffuse", dirComponent.diffuse);
        mat.shader->setVec3(headerUniform + "specular", dirComponent.specular);
        mat.shader->setVec3("viewPos",
                            g_coordinator.getComponent<Transform>(m_mainCamera).getLocalPosition());
        i++;
    }
    i = 0;
    for (const Entity &pointLightEnt : m_pointLights)
    {
        auto &lightComponent = g_coordinator.getComponent<PointLight>(pointLightEnt);
        auto &lightTransform = g_coordinator.getComponent<Transform>(pointLightEnt);
        bool animateLight = false;
        if (animateLight)
        {
            float distance = 5;
            float positionLightZ = distance * sin(static_cast<float>(glfwGetTime())) - 3;
            vec3_t lightOldPos = lightTransform.getLocalPosition();
            lightTransform.setLocalPosition(vec3_t(lightOldPos.x, lightOldPos.y, positionLightZ));
            // Log::debug("Lamp movement: " + std::to_string(positionLightZ));
        }

        // TODO(Pablo): allow this in a component? needs to be redone
        bool activateFunkyLights = false;
        if (activateFunkyLights)
        {
            auto &lightMat = g_coordinator.getComponent<Material>(pointLightEnt);
            glm::vec3 lightColor;
            lightColor.x = sin(static_cast<float>(glfwGetTime() * 2.0F));
            lightColor.y = sin(static_cast<float>(glfwGetTime() * 0.7F));
            lightColor.z = sin(static_cast<float>(glfwGetTime() * 1.3F));

            lightMat.diffuse = lightColor;
            lightComponent.diffuse = lightColor;
            lightComponent.specular = lightColor;
            // lightMat.ambient = lightMat.diffuse * glm::vec3(0.2f);
        }
        // TODO(Pablo): Should use global position
        std::string headerUniform{"pointLights[" + std::to_string(i) + "]."};
        mat.shader->setVec3(headerUniform + "position", lightTransform.getLocalPosition());
        mat.shader->setVec3(headerUniform + "ambient", lightComponent.ambient);
        mat.shader->setVec3(headerUniform + "diffuse", lightComponent.diffuse);
        mat.shader->setVec3(headerUniform + "specular", lightComponent.specular);
        mat.shader->setFloatUniform(headerUniform + "linear", lightComponent.linear);
        mat.shader->setFloatUniform(headerUniform + "quadratic", lightComponent.quadratic);
        mat.shader->setVec3("viewPos",
                            g_coordinator.getComponent<Transform>(m_mainCamera).getLocalPosition());
        i++;
    }

    i = 0;
    for (const Entity &lightEnt : m_spotLights)
    {
        auto &lightTransform = g_coordinator.getComponent<Transform>(lightEnt);
        auto &lightComponent = g_coordinator.getComponent<SpotLight>(lightEnt);
        // IMPORTANT PART
        mat.shader->useMaterialShader();
        std::string headerUniform{"spotLights[" + std::to_string(i) + "]."};
        // TODO(Pablo): should be global rotation and pos intead of local, transform need to have
        // those fields
        mat.shader->setVec3(headerUniform + "position", lightTransform.getLocalPosition());
        mat.shader->setVec3(headerUniform + "direction", lightTransform.getLocalRotationEuler());
        mat.shader->setVec3(headerUniform + "ambient", lightComponent.ambient);
        mat.shader->setVec3(headerUniform + "diffuse", lightComponent.diffuse);
        mat.shader->setVec3(headerUniform + "specular", lightComponent.specular);
        mat.shader->setFloatUniform(headerUniform + "linear", lightComponent.linear);
        mat.shader->setFloatUniform(headerUniform + "quadratic", lightComponent.quadratic);
        mat.shader->setFloatUniform(headerUniform + "cutoff", lightComponent.cutOff);
        mat.shader->setFloatUniform(headerUniform + "outerCutoff", lightComponent.outerCutoff);
        // TODO(Pablo): should be the global rotation in the case of the camera also.
        mat.shader->setVec3("viewPos",
                            g_coordinator.getComponent<Transform>(m_mainCamera).getLocalPosition());
        i++;
    }
}
void RenderSystem::prepareGeom()
{
    // TODO(Pablo): maybe this should be called each time a mesh is loaded
    for (Entity const &entity : mEntities)
    {
        auto &geom = g_coordinator.getComponent<Geometry>(entity);
        auto &ren = g_coordinator.getComponent<Renderable>(entity);

        createVAOs(&geom, &ren);  // create VAOs
    }
}

void RenderSystem::createVAOs(const Geometry *a_geomComp, Renderable *const a_renComp)
{
    // Note: glm::vec3 occupies 12 bytes even if there are 9+ terms defined inside its struct.
    // this is related with aliases
    // https://gamedev.stackexchange.com/questions/146238/why-is-the-size-of-glms-vec3-struct-12-bytes

    // Example size vertex: 32 bytes = float 4 bytes * (3 pos + 3normals, 2uvs)
    constexpr const int k_sizeVertexStructure = sizeof(Vertex);
    constexpr const int k_sizeFace = sizeof(face_t);
    const int sizeFaces = k_sizeFace * a_geomComp->faces.size();
    const int numVertices = a_geomComp->vertices.size();
    const GLsizeiptr sizeVerts = k_sizeVertexStructure * numVertices;
    // Since the struct groups all data in a consecutive sequence, we can use the first data pointer
    const void *dataPtr = &a_geomComp->vertices[0].pos.x;
    // copy vertices data in vertex buffer
    glGenVertexArrays(1, &a_renComp->VAO);
    glGenBuffers(1, &a_renComp->VBO);
    glGenBuffers(1, &a_renComp->EBO);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure
    // vertex attributes(s).
    glBindVertexArray(a_renComp->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, a_renComp->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeVerts, dataPtr, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a_renComp->EBO);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeFaces, &a_geomComp->faces[0], GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, pos));
    glEnableVertexAttribArray(0);

    // normals attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void *)offsetof(Vertex, normal));
    glEnableVertexAttribArray(1);

    // texture coord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, uv));
    glEnableVertexAttribArray(2);

    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex
    // attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS
    // stored in the VAO; keep the EBO bound.
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but
    // this rarely happens. Modifying other VAOs requires a call to glBindVertexArray anyways so we
    // generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
}

void RenderSystem::createDefaultTextureData()
{
    // TODO: I'm sure there is a better way to set a default white texture, improve it.
    for (Entity const &entity : mEntities)
    {
        unsigned int whiteTextureID =
            AssimpLoader::createPlainColorTexture(vec4_t(255, 255, 255, 255));
        unsigned int blackTextureID = AssimpLoader::createPlainColorTexture(vec4_t(0, 0, 0, 1));
        auto &mat = g_coordinator.getComponent<Material>(entity);
        /*for (auto & texture : mat.textures)
        {
                texture.id = whiteTextureID;
        }*/

        unsigned int diffuseNr = 1;
        unsigned int specularNr = 1;
        unsigned int normalNr = 1;
        unsigned int heightNr = 1;
        unsigned int emissiveNr = 1;
        for (auto &texture : mat.textures)
        {
            // retrieve texture number (the N in diffuse_textureN)
            std::string number;
            // std::string name =
            // TextureManager::textureTypeToStr(mat.textures[i]->getTextureType());
            TextureType textureType = texture->getTextureType();
            switch (textureType)
            {
                case TextureType::diffuse:
                    diffuseNr++;
                    break;
                case TextureType::specular:
                    specularNr++;
                    break;
                case TextureType::normal:
                    normalNr++;
                    break;
                case TextureType::height:
                    heightNr++;
                    break;
                case TextureType::emissive:
                    emissiveNr++;
                    break;
                default:
                    THULE_ASSERTION_ERROR("Error! texture type " +
                                          TextureManager::textureTypeToStr(textureType) +
                                          " was not contempled");
                    break;
            }
        }
        // if there are no textures of one type add a white 1x1 texture
        // if there are 1 texture, set the color to white
        if (diffuseNr == 1)
        {
            std::shared_ptr<OpenGLTexture2D> texturePtr = std::make_shared<OpenGLTexture2D>();
            texturePtr->setTexturePath("default_white_texture");
            texturePtr->setTextureID(whiteTextureID);
            texturePtr->setTextureType(TextureType::diffuse);
            mat.textures.emplace_back(texturePtr);
        }
        else
        {
            // set to white to avoid altering the texture color
            mat.diffuse = vec3_t(1, 1, 1);
            mat.ambient = vec3_t(1, 1, 1);
        }
        if (specularNr == 1)
        {
            std::shared_ptr<OpenGLTexture2D> texturePtr = std::make_shared<OpenGLTexture2D>();
            texturePtr->setTexturePath("default_white_texture");
            texturePtr->setTextureID(whiteTextureID);
            texturePtr->setTextureType(TextureType::specular);
            mat.textures.emplace_back(texturePtr);
        }
        else
        {
            mat.specular = vec3_t(1, 1, 1);
        }
        if (emissiveNr == 1)
        {
            // Since for now the emissive doesn't have an associated color, if is not present
            // we want to repace it with zeros (black textures)
            std::shared_ptr<OpenGLTexture2D> texturePtr = std::make_shared<OpenGLTexture2D>();
            texturePtr->setTexturePath("default_black_texture");
            texturePtr->setTextureID(blackTextureID);
            texturePtr->setTextureType(TextureType::emissive);
            mat.textures.emplace_back(texturePtr);
        }
    }
}