#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
//#include <glm/gtx/quaternion.hpp>
#include <string>

#include "glm/fwd.hpp"
using vec2_t = glm::vec2;
using face_t = glm::uvec3;
using vec3_t = glm::vec3;
using vec4_t = glm::vec4;
using quat_t = glm::quat;
// Is assumed that colors range from 0F to 1.0F unless the contrary is specified.
using color3_t = glm::vec3;
using normal_t = glm::vec3;
using uv_t = glm::vec2;
using mat4_t = glm::mat4;

struct ColoredVertex
{
    vec3_t pos;
    color3_t color;
    normal_t normal;
    uv_t uv;
    ColoredVertex()
        : pos{vec3_t(0, 0, 0)},
          color{color3_t(0, 0, 0)},
          normal{vec3_t(0, 0, 0)},
          uv{uv_t(0, 0)} {};
    ColoredVertex(vec3_t a_pos, color3_t a_color, normal_t a_normal, uv_t a_uv);
    // Vertex(glm::vec3 a_pos, glm::vec3 a_normal, glm::vec2 a_uv);
};
struct Vertex
{
    vec3_t pos;
    normal_t normal;
    uv_t uv;
    Vertex() : pos{vec3_t(0, 0, 0)}, normal{vec3_t(0, 0, 0)}, uv{uv_t(0, 0)} {};
    Vertex(vec3_t a_pos, normal_t a_normal, uv_t a_uv);
};
