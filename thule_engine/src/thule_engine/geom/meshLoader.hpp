#pragma once
#include <memory>
#include <string>

#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/geom/assimp_loader/assimpDef.hpp"
class MeshLoader
{
   public:
    static Entity loadMesh(std::string a_path);

   private:
    static Entity loadAssimpNode(const std::shared_ptr<assimp::Model>& a_modelPtr, Entity a_parent);
};