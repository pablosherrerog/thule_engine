#pragma once
#include <assimp/scene.h>

#include <map>
#include <memory>
#include <thule_engine/render/textures/texture.hpp>

#include "thule_engine/geom/assimp_loader/assimpDef.hpp"
#include "thule_engine/geom/geomDef.hpp"

class AssimpLoader
{
   public:
    static std::map<std::string, std::shared_ptr<Texture>>
        textures_loaded;  // stores all the textures loaded so far, optimization to make sure
                          // textures aren't loaded more than once.
    static std::shared_ptr<assimp::Model> loadGeom(std::string a_path);
    static unsigned int textureFromData(unsigned char* a_data, const uint32_t& a_width,
                                        const uint32_t& a_height, int a_numComponents);
    static unsigned int createPlainColorTexture(vec4_t a_color);

   private:
    static void processNode(std::shared_ptr<assimp::Model> a_ptrModel, aiNode* a_node,
                            const aiScene* a_scene, std::string a_directory_mesh);
    static std::shared_ptr<assimp::Mesh> processMesh(aiMesh* a_mesh, const aiScene* a_scene,
                                                     std::string a_directory_mesh);
    static std::vector<std::shared_ptr<Texture>> loadMaterialTextures(
        aiMaterial* a_aiMat, aiTextureType a_aiType, const std::string& a_meshDirectory);

    static TextureType aiTextureTypeToTextureType(aiTextureType a_aiTextureType);
};