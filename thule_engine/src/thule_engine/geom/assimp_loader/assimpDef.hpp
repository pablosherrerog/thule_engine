#pragma once
#include <glm/glm.hpp>
#include <memory>
#include <string>
#include <thule_engine/render/textures/texture.hpp>
#include <vector>

namespace assimp
{
struct Vertex
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;
    glm::vec3 tangent;
    glm::vec3 bitangent;
};

class Mesh
{
   public:
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<std::shared_ptr<Texture>> textures;
    float assimpColor[3];  // rgb
    float assimpAmbient[3];
    float assimpSpecular[3];
};

class Model
{
   public:
    std::string name;
    // texturesLoaded is used to store all the textures loaded so far. This is an optimization to
    // make sure textures aren't loaded more than once.
    std::vector<std::shared_ptr<Texture>> texturesLoaded;
    std::shared_ptr<Mesh> meshPtr{nullptr};
    std::vector<std::shared_ptr<Model>> children;
};

class Relationship
{
    uint16_t parent;
    std::vector<uint16_t> children;
};

}  // namespace assimp
