// extracted from https://learnopengl.com/Model-Loading/Model
#include "thule_engine/geom/assimp_loader/assimpLoader.hpp"

#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <glad/glad.h>

#include <assimp/Importer.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <thule_engine/common/exceptionUtils.hpp>
#include <thule_engine/common/log.hpp>
#include <thule_engine/common/utils.hpp>
#include <thule_engine/geom/assimp_loader/assimpDef.hpp>
#include <thule_engine/geom/geomDef.hpp>
#include <thule_engine/render/textures/texture.hpp>
#include <thule_engine/render/textures/textureManager.hpp>

// Sean Barret load texture
//#ifndef STB_IMAGE_IMPLEMENTATION
//#define STB_IMAGE_IMPLEMENTATION
#include "assimp/material.h"
#include "assimp/types.h"
#include "stb_image.h"
//#endif
#include <cstdint>
#include <filesystem>
#include <glm/gtx/string_cast.hpp>

// Map with texture paths to reuse same texture if it was already loaded.
std::map<std::string, std::shared_ptr<Texture>> AssimpLoader::textures_loaded;

std::shared_ptr<assimp::Model> AssimpLoader::loadGeom(std::string a_path)
{
    Assimp::Importer import;
    std::shared_ptr<assimp::Model> ptrModel(new assimp::Model());
    const aiScene *scene = import.ReadFile(a_path, aiProcess_Triangulate | aiProcess_FlipUVs);

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        Utils::printErrorAndExit("Error! trying to load " + a_path + " with assimp failed");
        return ptrModel;
    }
    std::string directory = a_path.substr(0, a_path.find_last_of('/'));

    processNode(ptrModel, scene->mRootNode, scene, directory);
    return ptrModel;
}

void AssimpLoader::processNode(std::shared_ptr<assimp::Model> a_ptrModel, aiNode *a_node,
                               const aiScene *a_scene, std::string a_directory_mesh)
{
    Log::debug("Loading assimp node " + (std::string)a_node->mName.data);
    a_ptrModel->name = (std::string)a_node->mName.data;
    if (a_node->mNumMeshes > 1)
    {
        // TODO: I'm not sure how to use Blender to emulate this, each mesh goes to a node, in the
        // future it could be added that the meshes were transfered to new created nodes but I'm not
        // completly sure if it could be a better alternative so for now the support will not be
        // present (maybe modify the Mesh component?)
        Utils::printErrorAndExit(
            "Error! currently our loader can't load several meshes in one node");
    }
    if (a_node->mNumMeshes > 0)
    {
        aiMesh *mesh = a_scene->mMeshes[a_node->mMeshes[0]];
        a_ptrModel->meshPtr = processMesh(mesh, a_scene, a_directory_mesh);
    }

    // do the same for each of its children
    for (unsigned int i = 0; i < a_node->mNumChildren; i++)
    {
        std::shared_ptr<assimp::Model> ptrChildrenModel(new assimp::Model());
        a_ptrModel->children.emplace_back(ptrChildrenModel);
        processNode(ptrChildrenModel, a_node->mChildren[i], a_scene, a_directory_mesh);
    }
}

std::shared_ptr<assimp::Mesh> AssimpLoader::processMesh(aiMesh *a_mesh, const aiScene *a_scene,
                                                        std::string a_directory_mesh)
{
    // data to fill
    std::vector<assimp::Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<std::shared_ptr<Texture>> textures;

    // walk through each of the mesh's vertices
    for (unsigned int i = 0; i < a_mesh->mNumVertices; i++)
    {
        assimp::Vertex vertex;
        glm::vec3 vector;  // we declare a placeholder vector since assimp uses its own vector class
                           // that doesn't directly convert to glm's vec3 class so we transfer the
                           // data to this placeholder glm::vec3 first.
        // positions
        vector.x = a_mesh->mVertices[i].x;
        vector.y = a_mesh->mVertices[i].y;
        vector.z = a_mesh->mVertices[i].z;
        vertex.position = vector;
        // normals
        if (a_mesh->HasNormals())
        {
            vector.x = a_mesh->mNormals[i].x;
            vector.y = a_mesh->mNormals[i].y;
            vector.z = a_mesh->mNormals[i].z;
            vertex.normal = vector;
        }

        // texture coordinates
        if (a_mesh->HasTextureCoords(0))  // does the mesh contain texture coordinates?
        {
            glm::vec2 vec;
            // a vertex can contain up to 8 different texture coordinates. We thus make the
            // assumption that we won't use models where a vertex can have multiple texture
            // coordinates so we always take the first set (0).
            vec.x = a_mesh->mTextureCoords[0][i].x;
            vec.y = a_mesh->mTextureCoords[0][i].y;
            vertex.texCoords = vec;

            // tangent
            // vector.x = a_mesh->mTangents[i].x;
            // vector.y = a_mesh->mTangents[i].y;
            // vector.z = a_mesh->mTangents[i].z;
            // vertex.tangent = vector;
            // bitangent
            // vector.x = a_mesh->mBitangents[i].x;
            // vector.y = a_mesh->mBitangents[i].y;
            // vector.z = a_mesh->mBitangents[i].z;
            // vertex.bitangent = vector;
        }
        else
            vertex.texCoords = glm::vec2(0.0f, 0.0f);

        vertices.emplace_back(vertex);
    }
    // now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the
    // corresponding vertex indices.
    for (unsigned int i = 0; i < a_mesh->mNumFaces; i++)
    {
        aiFace face = a_mesh->mFaces[i];

        // retrieve all indices of the face and store them in the indices vector
        for (unsigned int j = 0; j < face.mNumIndices; j++)
        {
            indices.emplace_back(face.mIndices[j]);
        }
    }
    // process materials
    aiMaterial *material = a_scene->mMaterials[a_mesh->mMaterialIndex];
    // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
    // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
    // Same applies to other texture as the following list summarizes:
    // diffuse: texture_diffuseN
    // specular: texture_specularN
    // normal: texture_normalN

    // Check http://forum.lwjgl.org/index.php?topic=6733.0
    aiColor3D color(0.f, 0.f, 0.f);
    material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
    aiColor3D ambient(0.f, 0.f, 0.f);
    material->Get(AI_MATKEY_COLOR_DIFFUSE, ambient);
    aiColor3D specular(0.f, 0.f, 0.f);
    material->Get(AI_MATKEY_COLOR_SPECULAR, specular);
    // 1. diffuse maps
    std::vector<std::shared_ptr<Texture>> diffuseMaps =
        loadMaterialTextures(material, aiTextureType_DIFFUSE, a_directory_mesh);
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
    // 2. specular maps
    std::vector<std::shared_ptr<Texture>> specularMaps =
        loadMaterialTextures(material, aiTextureType_SPECULAR, a_directory_mesh);
    textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
    // 3. normal maps
    std::vector<std::shared_ptr<Texture>> normalMaps =
        loadMaterialTextures(material, aiTextureType_NORMALS, a_directory_mesh);
    textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
    // 4. height maps
    std::vector<std::shared_ptr<Texture>> heightMaps =
        loadMaterialTextures(material, aiTextureType_HEIGHT, a_directory_mesh);
    textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
    // 5. emissive maps
    std::vector<std::shared_ptr<Texture>> emissiveMaps =
        loadMaterialTextures(material, aiTextureType_EMISSIVE, a_directory_mesh);
    textures.insert(textures.end(), emissiveMaps.begin(), emissiveMaps.end());

    // return a mesh object created from the extracted mesh data
    std::shared_ptr<assimp::Mesh> meshPtr(new assimp::Mesh());
    meshPtr->vertices = vertices;
    meshPtr->indices = indices;
    meshPtr->textures = textures;
    // float color_array[] = { color.r , color.g, color.b };
    meshPtr->assimpColor[0] = color.r;
    meshPtr->assimpColor[1] = color.g;
    meshPtr->assimpColor[2] = color.b;

    meshPtr->assimpAmbient[0] = ambient.r;
    meshPtr->assimpAmbient[1] = ambient.g;
    meshPtr->assimpAmbient[2] = ambient.b;

    meshPtr->assimpSpecular[0] = specular.r;
    meshPtr->assimpSpecular[1] = specular.g;
    meshPtr->assimpSpecular[2] = specular.b;
    return meshPtr;
}

TextureType AssimpLoader::aiTextureTypeToTextureType(aiTextureType a_aiTextureType)
{
    switch (a_aiTextureType)
    {
        case aiTextureType_DIFFUSE:
            return TextureType::diffuse;
            break;
        case aiTextureType_SPECULAR:
            return TextureType::specular;
            break;
        case aiTextureType_NORMALS:
            return TextureType::diffuse;
            break;
        case aiTextureType_HEIGHT:
            return TextureType::height;
            break;
        case aiTextureType_EMISSIVE:
            return TextureType::emissive;
            break;
        default:
            THULE_EXCEPTION(
                "Error! Name translation not supported, should be added to the switch statement");
    }
}
// checks all material textures of a given type and loads the textures if they're not loaded yet.
// the required info is returned as a Texture struct.
std::vector<std::shared_ptr<Texture>> AssimpLoader::loadMaterialTextures(
    aiMaterial *a_aiMat, aiTextureType a_aiType, const std::string &a_meshDirectory)
{
    std::vector<std::shared_ptr<Texture>> textures;
    for (unsigned int i = 0; i < a_aiMat->GetTextureCount(a_aiType); i++)
    {
        aiString aiPathTexture;
        a_aiMat->GetTexture(a_aiType, i, &aiPathTexture);
        std::string texturePath = std::string(aiPathTexture.C_Str());
        // If the file path doesn't exist check if it's relative to the mesh.
        if (!std::filesystem::exists(texturePath))
        {
            std::filesystem::path fullPath = a_meshDirectory;
            fullPath /= texturePath;
            texturePath = fullPath.string();
            if (!std::filesystem::exists(texturePath))
            {
                THULE_EXCEPTION("Error! the texture path " + texturePath + " doesn't exists");
            }
        }
        // check if texture was loaded before and if so, continue to next iteration: skip loading a
        // new texture
        if (textures_loaded.find(texturePath) != textures_loaded.end())
        {
            textures.emplace_back(textures_loaded[texturePath]);
        }
        else
        {  // if texture hasn't been loaded already, load it
            TextureType textureType = aiTextureTypeToTextureType(a_aiType);
            std::shared_ptr<Texture> texturePtr = TextureManager::create(texturePath, textureType);
            textures.emplace_back(texturePtr);
            // store it as texture loaded for entire model, to ensure we won't
            // unnecessary load duplicated textures.
            textures_loaded.insert(
                std::pair<std::string, std::shared_ptr<Texture>>(texturePath, texturePtr));
        }
    }

    return textures;
}

unsigned int AssimpLoader::textureFromData(unsigned char *a_data, const uint32_t &a_width,
                                           const uint32_t &a_height, int a_numComponents)
{
    if (!a_data)
    {
        THULE_EXCEPTION("Error! can't load texture data since it is null")
    }
    unsigned int textureID;
    // how many textures you need to generate (1) and the IDOpenGLTexture2D
    glGenTextures(1, &textureID);

    GLenum format;
    if (a_numComponents == 1)
        format = GL_RED;
    else if (a_numComponents == 3)
        format = GL_RGB;
    else if (a_numComponents == 4)
        format = GL_RGBA;
    else
    {
        Utils::printErrorAndExit("Error! texture has " + std::to_string(a_numComponents) +
                                 " texture components which is not supported");
    }
    // when we bind it so all next texture commands affect the bound texture
    glBindTexture(GL_TEXTURE_2D, textureID);
    // configure the wrapping/filtering options
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // load the texture data
    glTexImage2D(GL_TEXTURE_2D, 0, format, a_width, a_height, 0, format, GL_UNSIGNED_BYTE, a_data);
    // generate mipmaps (smaller version of the texture in case the camera is far away)
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);  // unbind texture

    return textureID;
}
// colors rgba from 0 to 255
unsigned int AssimpLoader::createPlainColorTexture(vec4_t a_color)
{
    unsigned char *data = new unsigned char[4 * sizeof(unsigned char)];
    data[0] = (unsigned char)(a_color.r);
    data[1] = (unsigned char)(a_color.g);
    data[2] = (unsigned char)(a_color.b);
    data[3] = (unsigned char)(a_color.a);
    return textureFromData(data, 1, 1, 4);
}
