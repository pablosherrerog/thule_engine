#pragma once
#include <vector>

#include "thule_engine/geom/geomDef.hpp"

class Square
{
   public:
    std::vector<Vertex> vertices;
    std::vector<face_t> indices;
    Square();
};
