#pragma once
#include <string>
//#include <glad/glad.h>
//#include <GLFW/glfw3.h>´
struct GLFWwindow;
class Event;
class Window
{
   public:
    Window(int a_width, int a_height, int a_posX, int a_posY, const std::string& a_name);

    void processInput();
    void onUpdate();
    // For now the window will be only a glfw window (but could be other type so the type will be a
    // void pointer). It should be casted to GLFWwindow to getting the correct data. (GLFWwindowPtr
    // = static_cast<GLFWwindow *>(glfwWindow)).
    void* getNativeWindow() { return m_glfwWindow; };
    float getWidth() const { return m_width; };
    float getHeight() const { return m_height; };
    void unlockCursorToWindow();
    void lockCursorToWindow();

   private:
    GLFWwindow* m_glfwWindow;
    float m_width = -1;
    float m_height = -1;
    bool m_cursorLockedToWindow = false;
    void onWindowsResize(const Event& a_event);
};
