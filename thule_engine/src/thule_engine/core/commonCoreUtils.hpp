#pragma once

#include <map>
#include <string>
#include <typeindex>

#include "thule_engine/common/exceptionUtils.hpp"
#include "thule_engine/core/metaprograming.hpp"

// Components
#include "thule_engine/core/ecs/componentsDefinitions.hpp"

using ComponentList =
    mp::TypeList<Metadata, Relationship, Transform, Renderable, Material, Geometry,
                 DirectionalLight, PointLight, SpotLight, Camera, SimpleAnimation>;

class CommonCoreUtils
{
   public:
    template <typename T>
    static std::string getComponentName()
    {
        if (!ComponentList::contains<T>())
        {
            THULE_EXCEPTION(
                "Error! you are trying to get the component name of a type that is not a "
                "component");
        }
        if (componentNames.find(typeid(T)) == componentNames.end())
        {
            THULE_ASSERTION_ERROR(
                "Error! the type name required is indeed a component but its name is not defined. "
                "Update the componentNames map with the name to fix this problem.")
        }
        return componentNames[typeid(T)];
    }
    template <typename T>
    static void throwExceptionIfNotComponent()
    {
        if (!ComponentList::contains<T>())
        {
            const char* typeID = typeid(T).name();
            std::string typeIDStr(typeID);
            THULE_EXCEPTION("Error! the type (with id " + typeIDStr +
                            ") you are trying to register is not a component.");
        }
    }

   private:
    static std::map<std::type_index, std::string> componentNames;
};