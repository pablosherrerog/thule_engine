#include "thule_engine/core/window.hpp"

#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include <functional>
#include <iostream>
#include <string>

#include "thule_engine/common/log.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/core/events/appEvents.hpp"
#include "thule_engine/core/events/event.hpp"
#include "thule_engine/core/events/keyboardEvents.hpp"
#include "thule_engine/core/events/mouseEvents.hpp"
#include "thule_engine/core/input/input.hpp"
#include "thule_engine/core/input/keyCodes.hpp"

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* a_window, int a_width, int a_height)
{
    Dispatcher::broadcast(app_events::WindowResizeEvent(a_width, a_height));
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    Dispatcher::broadcast(MouseEvents::ScrollEvent(xoffset, yoffset));
}

Window::Window(int a_width, int a_height, int a_posX, int a_posY, const std::string& a_name)
{
    m_width = a_width;
    m_height = a_height;

    // https://clang.llvm.org/extra/clang-tidy/checks/modernize-avoid-bind.html
    auto on_windows_resize = [&](const Event& a_event)
    {
        auto windowResizeEvent = static_cast<const app_events::WindowResizeEvent&>(a_event);

        m_width = windowResizeEvent.getWidth();
        m_height = windowResizeEvent.getHeight();
        // std::cout << "New window dimensions" << m_width << ", " << m_height << std::endl;
    };
    Dispatcher::subscribe(event_types::EventType::k_windowResize, on_windows_resize);
    // glfw window creation
    // --------------------
    m_glfwWindow = glfwCreateWindow(a_width, a_height, a_name.c_str(), nullptr, nullptr);
    if (m_glfwWindow == nullptr)
    {
        Utils::printErrorAndExit("Failed to create GLFW window");
    }
    glfwSetWindowPos(m_glfwWindow, a_posX, a_posY);
    glfwMakeContextCurrent(m_glfwWindow);
    glfwSetFramebufferSizeCallback(m_glfwWindow, framebuffer_size_callback);
    Dispatcher::post<app_events::WindowResizeEvent>(
        app_events::WindowResizeEvent(a_width, a_height));
    // hide mouse and prepare it to be used to rotate the camera
    glfwSetScrollCallback(m_glfwWindow, scroll_callback);
}

void Window::unlockCursorToWindow()
{
    Log::debug("Releasing the cursor from the window");
    glfwSetInputMode(m_glfwWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    m_cursorLockedToWindow = false;
}
void Window::lockCursorToWindow()
{
    Log::debug("Locking the cursor within the window");
    glfwSetInputMode(m_glfwWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    m_cursorLockedToWindow = true;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react
// accordingly
// ---------------------------------------------------------------------------------------------------------
void Window::onUpdate()
{
    PROFILER_SCOPED;
    glfwSwapBuffers(m_glfwWindow);
    glfwPollEvents();
    processInput();
}

void Window::processInput()
{
    // unity Input.GetKeyDown(KeyCode.LeftControl) == true
    if (glfwGetKey(m_glfwWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(m_glfwWindow, static_cast<int>(true));
}
