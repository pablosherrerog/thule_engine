#pragma once
#include <string>

namespace mp
{
template <typename T, typename U>
struct IsSame
{
    static constexpr bool value{false};
};
template <typename T>
struct IsSame<T, T>
{
    static constexpr bool value{true};
};
template <typename T, typename U>
constexpr bool isSameVar = IsSame<T, U>::value;

// from https://youtu.be/4NKbmCfZ9OI
template <typename... Ts>
struct TypeList
{
    constexpr static std::size_t size() noexcept { return sizeof...(Ts); }
    template <typename T>
    constexpr static bool contains() noexcept
    {
        // Fold expresion. This is equivalent to
        // return false || isSameVar<T, Ts1> || isSameVar<T, Ts2> || etc...
        return (false || ... || isSameVar<T, Ts>);
    }
};
template <typename T>
struct TypeId
{
    using type = T;
};
template <std::size_t N, typename... Ts>
struct NthType
{
    // If it enters this section without Ts it will mean out of range.
    static_assert(sizeof...(Ts) != 0, "Error! nth type out of range");
};
template <std::size_t N, typename... Ts>
using NthType_T = typename NthType<N, Ts...>::type;
// Specialized template, it will have preference over other less restrictive.
template <typename T, typename... Ts>
struct NthType<0, T, Ts...> : TypeId<T>
{
};

// N - 1 since we are not passing the T type to the new nth_type
template <std::size_t N, typename T, typename... Ts>
struct NthType<N, T, Ts...> : TypeId<NthType_T<N - 1, Ts...>>
{
};

// Didn't managed to integrate Typelist as input for NthType due to problem with ambiguous partial
// specializations
template <std::size_t N, typename... Ts>
struct NthTypeTypeList
{
};
template <std::size_t N, typename... Ts>
struct NthTypeTypeList<N, mp::TypeList<Ts...>> : TypeId<NthType_T<N, Ts...>>
{
};
// Interesting example to get the index from a Typelist.
// https://stackoverflow.com/questions/41579477/getting-index-of-a-type-in-a-typelist#comment70365221_41579477

}  // namespace mp