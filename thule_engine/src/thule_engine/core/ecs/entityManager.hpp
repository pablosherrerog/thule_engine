#pragma once
// extracted from https://code.austinmorlan.com/austin/ecs
#include <array>
#include <bitset>
#include <queue>

#include "thule_engine/core/ecs/typesECS.hpp"

class EntityManager
{
   private:
    std::bitset<MAX_ENTITIES> m_entities{0U};  // 1 if the entity exists, 0 if it doesn't.
    std::queue<Entity> removedEntitiesAvailable{};
    Entity m_nextSequentialEntityAvailable =
        0;  // is the next pos in the array free if nothing is removed
    int m_numEntitiesActive = 0;
    std::array<Signature, MAX_ENTITIES> m_Signatures{};

   public:
    EntityManager() = default;
    Entity createEntity();
    void removeEntity(Entity a_entityToRemove);
    bool isEntityInRange(Entity a_entity);
    int getActiveEntities();
    void setSignature(Entity entity, Signature signature);
    Signature getSignature(Entity entity);
    bool entityExists(Entity a_entity);
    void resetSignature(Entity a_entity);
};
