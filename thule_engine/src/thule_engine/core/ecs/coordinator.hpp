#pragma once
// Used https://code.austinmorlan.com/austin/2019-ecs/src/branch/master/Source/Core/Coordinator.hpp
// as reference.
#include <stdint.h>

#include <iostream>
#include <memory>
#include <thule_engine/common/exceptionUtils.hpp>
#include <thule_engine/common/log.hpp>
#include <thule_engine/common/utils.hpp>
#include <thule_engine/components/metadata.hpp>
#include <thule_engine/components/relationship.hpp>
#include <thule_engine/core/commonCoreUtils.hpp>
#include <thule_engine/core/ecs/componentManager.hpp>
#include <thule_engine/core/ecs/entityManager.hpp>
#include <thule_engine/core/ecs/systemManager.hpp>
#include <thule_engine/core/ecs/typesECS.hpp>
#include <vector>

/**/
#include <thule_engine/core/commonCoreUtils.hpp>
#include <thule_engine/core/metaprograming.hpp>

/**/
class Coordinator
{
   public:
    Coordinator()
    {
        Log::info("Initalizing coordinator");
        mComponentManager = std::make_unique<ComponentManager>();
        mEntityManager = std::make_unique<EntityManager>();
        mSystemManager = std::make_unique<SystemManager>();
    }

    Entity createEntity(Entity a_parent) { return createEntity(a_parent, ""); }
    Entity createEntity(const std::string& a_name)
    {
        // if the entity has no parents
        return createEntity(NULL_ENTITY, a_name);
    }
    Entity createEntity()
    {
        // if the entity has no parents
        return createEntity(NULL_ENTITY, "");
    }
    // Entity methods
    Entity createEntity(Entity a_parent, const std::string& a_name)
    {
        Entity entityToAdd = mEntityManager->createEntity();

        // All entities will have implicitly a relationship component to go through the hierarchy as
        // well as metadata
        Metadata metadata;
        if (a_name.empty())
        {
            metadata.name = "Entity " + std::to_string(entityToAdd);
        }
        else
        {
            metadata.name = a_name;
        }
        addComponent<Metadata>(entityToAdd, metadata);
        Relationship relChild;
        addComponent<Relationship>(entityToAdd, relChild);
        setParentEntity(entityToAdd, a_parent);
        return entityToAdd;
    }

    void destroyEntity(Entity a_entity)
    {
        errorOnNonExistingEntity(a_entity);
        // Access entity to remove parent and siblings relationship.
        setEntityAsRootEntity(a_entity);
        // Access children to remove them
        for (auto& child : getChildren(a_entity))
        {
            destroyEntity(child);
        }
        // Finish removing entity associated components data.
        mEntityManager->removeEntity(a_entity);

        mComponentManager->EntityDestroyed(a_entity);

        mSystemManager->EntityDestroyed(a_entity);

        m_rootEntities.erase(a_entity);
    }

    // Component methods
    template <typename T>
    void registerComponent()
    {
        mComponentManager->RegisterComponent<T>();
    }

    template <typename T>
    void addComponent(Entity entity, const T& component)
    {
        // TODO: raise error if there is a component of the same type defined
        if (!mComponentManager->checkIfComponentRegistered<T>())
        {
            registerComponent<T>();
        }

        mComponentManager->AddComponent<T>(entity, component);

        auto signature = mEntityManager->getSignature(entity);
        // the first argument is the position of the bitset and the second the value (true = 1)
        signature.set(mComponentManager->GetComponentType<T>(), true);
        mEntityManager->setSignature(entity, signature);

        mSystemManager->EntitySignatureChanged(entity, signature);
    }
    // Initialize with default component
    template <typename T>
    void addComponent(Entity entity)
    {
        addComponent<T>(entity, T());
    }

    template <typename T>
    void removeComponent(Entity entity)
    {
        mComponentManager->RemoveComponent<T>(entity);

        auto signature = mEntityManager->getSignature(entity);
        signature.set(mComponentManager->GetComponentType<T>(), false);
        mEntityManager->setSignature(entity, signature);

        mSystemManager->EntitySignatureChanged(entity, signature);
    }

    template <typename T>
    void resetComponent(Entity a_entity)
    {
        T& component = getComponent<T>(a_entity);
        component = T();
    }
    template <typename T>
    T& getComponent(Entity entity) const
    {
        if (entity == NULL_ENTITY)
        {
            THULE_ASSERTION_ERROR("Error! you are trying to get a component from a NULL_ENTITY");
        }
        return mComponentManager->GetComponent<T>(entity);
    }

    template <typename T>
    bool hasComponent(Entity a_entity)
    {
        return mComponentManager->hasComponent<T>(a_entity);
    }

    template <typename T>
    ComponentType getComponentType()
    {
        return mComponentManager->GetComponentType<T>();
    }

    // System methods

    // Requires the needed components by the system. So entities with those components are set
    // to be processed by the system.
    template <typename SystemType, typename... ComponentTypes>
    std::shared_ptr<SystemType> registerSystem()
    {
        // The signature is a bitset which identifies with 1 the components.
        // get component type returns a 0 if the component was registered first, 1 if second,
        // etc.. set sets the specified position of the bit array to 1, set(1) = 0 0 1 0
        std::shared_ptr<SystemType> systemPtr = mSystemManager->RegisterSystem<SystemType>();
        Signature signature;
        (..., signature.set(getComponentType<ComponentTypes>()));  // Fold expresion
        mSystemManager->SetSignature<SystemType>(signature);
        return systemPtr;
    }

    Signature getEntitySignature(Entity a_entity) { return mEntityManager->getSignature(a_entity); }

    // Entity will not have parent and siblings will be updated. The children are kept.
    void setEntityAsRootEntity(Entity a_child)
    {
        /*
             A
            / \
           B   C
          /     \
         E       D
         To
             A  C
            /    \
           B      D
          /
         E
        */

        auto& childRelationship = getComponent<Relationship>(a_child);
        // Remove the child from his parent
        if (childRelationship.getParent() != NULL_ENTITY)
        {
            auto& parentRelationship = getComponent<Relationship>(childRelationship.getParent());
            if (parentRelationship.first_child == a_child)
            {
                parentRelationship.first_child = childRelationship.next_sibling;
            }
            if (parentRelationship.last_child == a_child)
            {
                parentRelationship.last_child = childRelationship.prev_sibling;
            }
            parentRelationship.num_children--;
        }
        // Update their siblings like he didn't exist.
        if (childRelationship.prev_sibling != NULL_ENTITY)
        {
            auto& prevSiblingRel = getComponent<Relationship>(childRelationship.prev_sibling);
            prevSiblingRel.next_sibling = childRelationship.next_sibling;
        }
        if (childRelationship.next_sibling != NULL_ENTITY)
        {
            auto& nextSiblingRel = getComponent<Relationship>(childRelationship.next_sibling);
            nextSiblingRel.prev_sibling = childRelationship.prev_sibling;
        }
        // Remove their siblings and parent if any.
        childRelationship.prev_sibling = NULL_ENTITY;
        childRelationship.next_sibling = NULL_ENTITY;
        childRelationship.setParent(NULL_ENTITY);
        // Since it has not parent is a root entity
        m_rootEntities.insert(a_child);
    }
    void setParentEntity(Entity a_child, Entity a_parent)
    {
        /* e.g setParentEntity(C, B)
             A
            / \
           B   C
          /     \
         E       D
        To
             A
            /
           B
          / \
         E   C
              \
               D
        */
        auto& childRelationship = getComponent<Relationship>(a_child);
        // Remove the old parent and old siblings
        setEntityAsRootEntity(a_child);
        // Update the child parent
        childRelationship.setParent(a_parent);
        // Update the new parent and his last child.
        if (a_parent != NULL_ENTITY)
        {
            auto& newParentRelationship = getComponent<Relationship>(a_parent);
            // Add child as the last child (udpate the previous last child)
            if (newParentRelationship.last_child != NULL_ENTITY)
            {
                auto& oldLastChild = getComponent<Relationship>(newParentRelationship.last_child);
                oldLastChild.next_sibling = a_child;
                childRelationship.prev_sibling = newParentRelationship.last_child;
            }
            else
            {
                // If it entered here it means that the parent didn't have any children.
                newParentRelationship.first_child = a_child;
            }
            newParentRelationship.last_child = a_child;
            newParentRelationship.num_children++;
            // If the child was a root entity before and not anymore update root entities.
            if (m_rootEntities.count(a_child) == 1)
            {
                m_rootEntities.erase(a_child);
            }
        }
    }
    enum TreeTransverseOrder
    {
        k_preorder,
        k_postorder

    };
    void getEntitiesFromTree(Entity a_rootSubtree, std::vector<Entity>& a_out_entities,
                             TreeTransverseOrder a_treeTransverseOrder) const
    {
        // https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
        // Note that unless inorder is implented both preorder and postorder doesn't uniquely
        // identify the scene hierarchy.
        if (a_rootSubtree == NULL_ENTITY)
        {
            for (Entity rootEntity : getRootEntities())
            {
                getEntitiesFromTree(rootEntity, a_out_entities, a_treeTransverseOrder);
            }
        }
        else
        {
            switch (a_treeTransverseOrder)
            {
                case k_preorder:
                    // Visting parent before than children
                    a_out_entities.emplace_back(a_rootSubtree);

                    for (const Entity& child : getChildren(a_rootSubtree))
                    {
                        getEntitiesFromTree(child, a_out_entities, a_treeTransverseOrder);
                    }
                    break;
                case k_postorder:
                    // Visiting children before parent
                    for (const Entity& child : getChildren(a_rootSubtree))
                    {
                        getEntitiesFromTree(child, a_out_entities, a_treeTransverseOrder);
                    }
                    a_out_entities.emplace_back(a_rootSubtree);
                    break;
                default:
                    THULE_ASSERTION_ERROR("Error! the transverse order is not valid");
                    break;
            }
        }
    }
    void getEntitiesFromTree(Entity a_rootSubtree, std::vector<Entity>& a_out_entities) const
    {
        getEntitiesFromTree(a_rootSubtree, a_out_entities, k_preorder);
    }
    std::set<Entity> getRootEntities() const { return m_rootEntities; }

    std::vector<Entity> getChildren(Entity a_entity) const
    {
        const auto& relationship = getComponent<Relationship>(a_entity);
        std::vector<Entity> children;
        children.resize(relationship.num_children);
        Entity currentChild = relationship.first_child;
        int i = 0;
        while (currentChild != NULL_ENTITY)
        {
            children[i] = currentChild;
            currentChild = getComponent<Relationship>(currentChild).next_sibling;
            i++;
        }
        return children;
    }
    template <int N>
    void cloneComponent(Entity a_originalEntity, Entity a_clonedEntity)
    {
        using ComponentType = typename mp::NthTypeTypeList<N, ComponentList>::type;

        if (mp::isSameVar<ComponentType, Relationship>)
        {
            return;
        }
        if (mp::isSameVar<ComponentType, Metadata>)
        {
            // metadata is added by default while creating the entity so no need to add it
            // TODO: ideally we would generate cube_cloned, cube_cloned(1), cube_cloned(2)
            // etc...
            //  but this requires other type of funcionality (e.g searching same name in
            //  siblings)
            auto& component = getComponent<Metadata>(a_clonedEntity);
            std::string originalName = getComponent<Metadata>(a_originalEntity).name;
            component.name = originalName + "_cloned (ent " + std::to_string(a_clonedEntity) + ")";
            return;
        }
        if (hasComponent<ComponentType>(a_originalEntity))
        {
            //  Will call the copy constructor. E.g Camera& newComponent would not call it.
            ComponentType newComponent = getComponent<ComponentType>(a_originalEntity);
            addComponent<ComponentType>(a_clonedEntity, newComponent);
        }
    }

    // We create a struct due to problems with function specialization
    // https://stackoverflow.com/questions/6623375/c-template-specialization-on-functions
    template <int N>
    void cloneEntityComponents(Entity a_originalEntity, Entity a_clonedEntity)
    {
        // Loop through call componenents and copy the ones that are present in the entity to
        // clone.
        cloneComponent<N>(a_originalEntity, a_clonedEntity);
        // contexpr allows us to do this recursion at compile time. Available from c++17
        if constexpr (N > 0)
        {
            cloneEntityComponents<N - 1>(a_originalEntity, a_clonedEntity);
        }
    }

    Entity cloneEntity(Entity a_originalEntity)
    {
        return cloneEntity(a_originalEntity, NULL_ENTITY);
    }

    Entity cloneEntity(Entity a_originalEntity, Entity a_parentEntity)
    {
        Entity clonedEntity = createEntity(a_parentEntity);
        auto originalEntityStr = getComponent<Metadata>(a_originalEntity).name;
        std::vector<Entity> children = getChildren(a_originalEntity);
        for (Entity& originalChild : getChildren(a_originalEntity))
        {
            cloneEntity(originalChild, clonedEntity);
        }
        // Will start from the last component type and will clone it if it's
        // present in the original entity (will do this process for all component types).
        cloneEntityComponents<ComponentList::size() - 1>(a_originalEntity, clonedEntity);
        return clonedEntity;
    }

    bool entityExists(Entity a_entity) { return mEntityManager->entityExists(a_entity); }
    bool errorOnNonExistingEntity(Entity a_entity)
    {
        if (!entityExists(a_entity))
            THULE_EXCEPTION("Error! entity (" + std::to_string(a_entity) +
                            ") "
                            "doesn't exist");
    }

   private:
    std::unique_ptr<ComponentManager> mComponentManager;
    std::unique_ptr<EntityManager> mEntityManager;
    std::unique_ptr<SystemManager> mSystemManager;
    std::set<Entity> m_rootEntities;
};
