#pragma once
// extracted from https://code.austinmorlan.com/austin/ecs
#include <thule_engine/common/exceptionUtils.hpp>

#include "thule_engine/common/log.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/core/commonCoreUtils.hpp"
#include "thule_engine/core/ecs/componentArray.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"

//#include <any>
#include <memory>  // unique ptr
#include <stdexcept>
#include <string>
#include <unordered_map>
class ComponentManager
{
   public:
    template <typename T>
    void RegisterComponent()
    {
        // Maybe instead of the typeid the typelist order could be used.
        const char* typeID = typeid(T).name();

        CommonCoreUtils::throwExceptionIfNotComponent<T>();
        if (mComponentTypes.find(typeID) != mComponentTypes.end())
        {
            THULE_EXCEPTION("Component " + CommonCoreUtils::getComponentName<T>() +
                            " was already registered");
        }

        mComponentTypes.insert({typeID, mNextComponentType});
        mComponentArrays.insert({typeID, std::make_shared<ComponentArray<T>>()});
        ++mNextComponentType;
    }
    template <typename T>
    bool inline checkIfComponentRegistered()
    {
        const char* typeID = typeid(T).name();
        CommonCoreUtils::throwExceptionIfNotComponent<T>();
        return mComponentTypes.find(typeID) != mComponentTypes.end();
    }
    template <typename T>
    ComponentType GetComponentType()
    {
        if (!checkIfComponentRegistered<T>())
        {
            RegisterComponent<T>();
        }
        const char* typeID = typeid(T).name();
        return mComponentTypes[typeID];
    }

    template <typename T>
    void AddComponent(Entity entity, const T& component)
    {
        if (!checkIfComponentRegistered<T>())
        {
            RegisterComponent<T>();
        }
        GetComponentArray<T>()->InsertData(entity, component);
    }

    template <typename T>
    void RemoveComponent(Entity entity)
    {
        if (!checkIfComponentRegistered<T>())
        {
            RegisterComponent<T>();
        }
        GetComponentArray<T>()->RemoveData(entity);
    }

    template <typename T>
    T& GetComponent(Entity entity)
    {
        if (!checkIfComponentRegistered<T>())
        {
            RegisterComponent<T>();
        }
        return GetComponentArray<T>()->GetData(entity);
    }

    template <typename T>
    bool hasComponent(Entity a_entity)
    {
        if (!checkIfComponentRegistered<T>())
        {
            RegisterComponent<T>();
        }
        return GetComponentArray<T>()->hasComponent(a_entity);
    }

    void EntityDestroyed(Entity entity)
    {
        for (auto const& pair : mComponentArrays)
        {
            auto const& component = pair.second;

            component->EntityDestroyed(entity);
        }
    }

   private:
    std::unordered_map<const char*, ComponentType> mComponentTypes{};
    std::unordered_map<const char*, std::shared_ptr<IComponentArray>> mComponentArrays{};
    ComponentType mNextComponentType{};

    template <typename T>
    std::shared_ptr<ComponentArray<T>> GetComponentArray()
    {
        // If it was not registered it will register the component.
        if (!checkIfComponentRegistered<T>())
        {
            RegisterComponent<T>();
        }
        const char* typeName = typeid(T).name();
        return std::static_pointer_cast<ComponentArray<T>>(mComponentArrays[typeName]);
    }
};
