#pragma once
// extracted from https://code.austinmorlan.com/austin/ecs
#include <set>

#include "typesECS.hpp"

class System
{
   public:
    // All entities which have the required components by the system will be stored inside
    // mEntities.
    std::set<Entity> mEntities;
};
