#include "thule_engine/core/application.hpp"

#include <GLFW/glfw3.h>  // to define context, manage user input and window creation. Alternatives (glut, sdl, sfml)
#include <glad/glad.h>  // since OpenGL in not a library is a standard/specification each drive has different implementation on how to access the data, so the location where the functions are stored is not know at compile time but in run-time. Instead of searching manually each function point  GLAD does that for us

#include "thule_engine/common/profiler.hpp"
#include "thule_engine/core/events/event.hpp"
#include "thule_engine/core/events/keyboardEvents.hpp"
#include "thule_engine/core/input/input.hpp"

#ifdef WIN32
#include <windows.h>  // To get the windows console
#endif

#include <memory>  // for smart pointers
#include <string>
#include <thule_engine/core/events/appEvents.hpp>
#include <thule_engine/core/events/engineEvents.hpp>
#include <thule_engine/core/events/event.hpp>

#include "thule_engine/common/log.hpp"
#include "thule_engine/common/pathsDefinitions.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/core/ecs/coordinator.hpp"
#include "thule_engine/core/layer.hpp"

Coordinator g_coordinator;  // you only need to define the global variable once (the rest of the
                            // .cpp should use extern Coordinator g_coordinator).

// TODO: not sure how to move this function, tried an static function in
//  Input.cpp but got an error

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        Dispatcher::broadcast(KeyboardEvents::KeyDown(key));
    }
}

Application *Application::m_appPtr = nullptr;
Application::Application(const std::string &a_name, const float &a_windowsWidth,
                         const float &a_windowsHeight)
    : m_name(a_name), m_lastFrameTimeSec(0)
{
    Log::info("Starting application " + a_name + " ");

    Log::info(
        "Instructions: press the second click in the viewer and use WASD to move around the "
        "scene.");

    if (m_appPtr != nullptr)
    {
        Utils::printErrorAndExit("Error! you are trying to create two apps which is not accepted");
    }
    m_appPtr = this;
#ifdef WIN32
    // Move windows console
    HWND consoleWindow = GetConsoleWindow();

    SetWindowPos(consoleWindow, 0, 0, 600, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#endif
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);  // use core profile to avoid loading unneeded old
                                               // functionality
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT,
                   GL_TRUE);  // uncomment this statement to fix compilation on OS X
#endif

    // Create window
    m_windowPtr = std::make_unique<Window>(a_windowsWidth, a_windowsHeight, 0, 32, "Main window");

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        Utils::printErrorAndExit("Failed to initialize GLAD");
    }
    printf("Opengl version used %s\n", glGetString(GL_VERSION));

    // configure global opengl state
    // -----------------------------
    glEnable(GL_DEPTH_TEST);

    common::Definitions::initializeDefinitions();
    m_running = true;
    glfwSetKeyCallback(static_cast<GLFWwindow *>(getWindow().getNativeWindow()), keyCallback);
}

// Will take ownership of the layer unique ptr. When the application is destroyed it will destroy
// the unique ptrs.
void Application::pushLayer(std::unique_ptr<Layer> a_layer)
{
    m_layers.emplace_back(std::move(a_layer))->onAttach();
}
void Application::close() { m_running = false; }
void Application::run()
{
    while (m_running)
    {
        auto timeSec = static_cast<float>(glfwGetTime());
        float timeStepSec = timeSec - m_lastFrameTimeSec;
        m_lastFrameTimeSec = timeSec;
        m_secondsSinceLastFPSUpdate += timeStepSec;
        if (m_secondsSinceLastFPSUpdate > k_fpsUpdateRateInSeconds)
        {
            Dispatcher::broadcast(engine_events::FPSUpdated(1 / timeStepSec));
            m_secondsSinceLastFPSUpdate = 0;
        }
        // TODO(Pablo): probably the the managment of deleted entities could be
        //  done in other place
        std::queue<engine_events::DeleteEntityEvent> deletedEntitiesEvents =
            Dispatcher::getPostedEvent<engine_events::DeleteEntityEvent>();
        while (!deletedEntitiesEvents.empty())
        {
            engine_events::DeleteEntityEvent deletedEntityEvent = deletedEntitiesEvents.front();
            Entity deletedEntity = deletedEntityEvent.getEntity();
            g_coordinator.destroyEntity(deletedEntity);
            deletedEntitiesEvents.pop();
            Dispatcher::broadcast(app_events::DeselectEntity(deletedEntity));
        }
        for (const std::unique_ptr<Layer> &layer : m_layers)
        {
            // TODO (Pablo): review, should I calculate the delta time inside the loop to take
            // into account the time spent in other layers? or all the layers should have the
            // same delta time.
            layer->onUpdate(timeStepSec);
        }
        m_windowPtr->onUpdate();
        if (glfwWindowShouldClose(static_cast<GLFWwindow *>(m_windowPtr->getNativeWindow())))
        {
            close();
        }
        PROFILER_FRAME_MARK;
    }
}