#include "event.hpp"

#include <iostream>
#include <memory>
#include <vector>

std::map<event_types::EventType, std::vector<Dispatcher::SlotType>> Dispatcher::m_observers;

void Dispatcher::subscribe(const event_types::EventType &descriptor, Dispatcher::SlotType &&slot)
{
    Dispatcher::m_observers[descriptor].emplace_back(slot);
}

// Call all subscribers of that particular event inmediately
void Dispatcher::broadcast(const Event &event)
{
    event_types::EventType type = event.type();
    // If nobody has subscribed to receive a notification ignore the event.
    if (Dispatcher::m_observers.count(type) == 0)
    {
        return;
    }
    // auto&& observers = m_observers.at(type);
    const std::vector<Dispatcher::SlotType> &observersFunctions = Dispatcher::m_observers.at(type);
    for (const Dispatcher::SlotType &observerFunction : observersFunctions)
    {
        observerFunction(event);
    }
}