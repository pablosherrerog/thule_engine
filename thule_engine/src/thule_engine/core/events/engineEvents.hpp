#pragma once
#include <cstddef>
#include <memory>
#include <string>
#include <thule_engine/core/ecs/componentsDefinitions.hpp>
#include <thule_engine/core/events/event.hpp>
#include <thule_engine/render/framebuffer.hpp>

#include "thule_engine/core/ecs/typesECS.hpp"

namespace engine_events
{
class DeleteEntityEvent : public Event
{
   public:
    explicit DeleteEntityEvent(Entity a_entity) : m_entity(a_entity) {}

    EVENT_CLASS_TYPE(k_entityDeleted);

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "DeleteEntityEvent: " << m_entity;
        return ss.str();
    }

    event_types::EventType type() const override { return eventType; }
    Entity getEntity() const { return m_entity; }

   private:
    unsigned int m_entity;
};
class FramebufferInitialization : public Event
{
   public:
    explicit FramebufferInitialization(std::shared_ptr<Framebuffer> a_framebufferPtr)
        : m_framebufferPtr(a_framebufferPtr)
    {
    }

    EVENT_CLASS_TYPE(k_framebufferInitialization);

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "FramebufferInitialization: " << m_framebufferPtr;
        return ss.str();
    }

    event_types::EventType type() const override { return eventType; }
    std::shared_ptr<Framebuffer> getFramebufferPtr() const { return m_framebufferPtr; }

   private:
    std::shared_ptr<Framebuffer> m_framebufferPtr = nullptr;
};
class FPSUpdated : public Event
{
   public:
    explicit FPSUpdated(float a_fps) : m_fps(a_fps) {}

    EVENT_CLASS_TYPE(k_fpsUpdated);

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "FPSUpdated: " << m_fps;
        return ss.str();
    }

    event_types::EventType type() const override { return eventType; }
    const float& getFPS() const { return m_fps; }

   private:
    float m_fps{0};
};

}  // namespace engine_events