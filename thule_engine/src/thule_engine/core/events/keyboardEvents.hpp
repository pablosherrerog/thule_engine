#include <string>

#include "event.hpp"
#include "thule_engine/core/input/keyCodes.hpp"
namespace KeyboardEvents
{
class KeyDown : public Event
{
   public:
    KeyDown(KeyCode a_keyDown) : m_keyDown(a_keyDown) {}
    KeyCode getKeyDown() const { return m_keyDown; }

    EVENT_CLASS_TYPE(k_keyboard);

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "Key down code " << m_keyDown;
        return ss.str();
    }

    event_types::EventType type() const override { return event_types::EventType::k_keyboard; }

   private:
    KeyCode m_keyDown;
};
}  // namespace KeyboardEvents