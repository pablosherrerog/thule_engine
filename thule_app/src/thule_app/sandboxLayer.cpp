#include "thule_app/sandboxLayer.hpp"

#include <memory>

#include "thule_engine/common/log.hpp"
#include "thule_engine/common/pathsDefinitions.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/timer.hpp"
#include "thule_engine/components/simpleAnimation.hpp"
#include "thule_engine/components/transform.hpp"
#include "thule_engine/core/ecs/coordinator.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/core/layer.hpp"
#include "thule_engine/geom/geomDef.hpp"
#include "thule_engine/geom/geomResources.hpp"
#include "thule_engine/geom/meshLoader.hpp"
#include "thule_engine/systems/cameraControlSystem.hpp"
#include "thule_engine/systems/renderSystem.hpp"
#include "thule_engine/systems/simpleAnimSystem.hpp"
#include "thule_engine/systems/transformationUpdaterSystem.hpp"

// Components
#include "thule_engine/core/ecs/componentsDefinitions.hpp"

extern Coordinator g_coordinator;
Entity SandboxLayer::m_mainCameraEnt = NULL_ENTITY;
SandboxLayer::SandboxLayer() : Layer("SandboxLayer") {}
void SandboxLayer::initScene()
{
    m_renderSystemPtr = g_coordinator.registerSystem<RenderSystem, Material, Transform, Geometry,
                                                     Renderable, Relationship>();

    m_cameraControlSystemPtr =
        g_coordinator.registerSystem<CameraControlSystem, Transform, Camera, Relationship>();
    m_transformationUpdaterSystemPtr =
        g_coordinator.registerSystem<TransformationUpdaterSystem, Transform, Relationship>();
    m_simpleAnimationSystemPtr =
        g_coordinator.registerSystem<SimpleAnimSystem, Transform, SimpleAnimation>();

    {
        // Create main camera entity.
        Entity entCam = g_coordinator.createEntity(NULL_ENTITY, "camera_1");
        m_mainCameraEnt = entCam;
        // Add components to camera
        g_coordinator.addComponent<Transform>(entCam, Transform{});
        g_coordinator.addComponent<Camera>(entCam, Camera{});

        auto &cameraTransform = g_coordinator.getComponent<Transform>(
            entCam);  // use the & to reference the same transfor var
        cameraTransform.translate(vec3_t(0, 4, 11));  // initial camera position
    }
}

void SandboxLayer::testQuad()
{
    // If everything breaks just test the simplest geometry, just two triangles.
    Entity entSquare1 = g_coordinator.createEntity();

    g_coordinator.addComponent<Transform>(entSquare1, Transform{});

    Geometry geomSquare = Geometry();
    geomSquare.vertices = Square().vertices;
    geomSquare.faces = Square().indices;

    g_coordinator.addComponent<Geometry>(entSquare1, geomSquare);

    Material mat1 = Material();
    mat1.shader =
        std::make_shared<Shader>(common::Definitions::m_shadersFolderPath + "/simpleShader.vert",
                                 common::Definitions::m_shadersFolderPath + "/simpleShader.frag");

    Renderable ren1 = Renderable();
    g_coordinator.addComponent<Material>(entSquare1, mat1);
    g_coordinator.addComponent<Renderable>(entSquare1, ren1);
}

void SandboxLayer::onAttach()
{
    // Register all components and systems that will be used
    initScene();
    loadPredefinedScene(2);
    // Set needed entities for render system
    m_cameraControlSystemPtr->init();
    m_renderSystemPtr->initializeRenderer();
    m_renderSystemPtr->updateMainCamera(m_mainCameraEnt);
    m_renderSystemPtr->updateDirLights(m_directionalLights);
    m_renderSystemPtr->updatePointLights(m_pointLights);
    m_renderSystemPtr->updateSpotLights(m_spotLights);
    // TODO(Pablo): Could be improved. Now we need to call prepareGeom and createDefaultTextureData
    // AFTER loading geometry.
    m_renderSystemPtr->prepareGeom();  // load geom in VAOs
    m_renderSystemPtr->createDefaultTextureData();
}

void SandboxLayer::onUpdate(float a_timeStepSec)
{
    PROFILER_SCOPED;

    // TODO(Pablo): Think something to launch all systems update in a loop.
    //  input
    // -----
    m_cameraControlSystemPtr->update(a_timeStepSec);
    m_transformationUpdaterSystemPtr
        ->update();  // before render update global transformation matrices if needed
    m_simpleAnimationSystemPtr->update(a_timeStepSec);

    m_renderSystemPtr->update();
    // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
    m_simpleAnimationSystemPtr->update(a_timeStepSec);
}
void SandboxLayer::loadPredefinedScene(int a_sceneId)
{
    // TODO(Pablo): this should be loaded from a json.
    switch (a_sceneId)
    {
        case 0:
            testQuad();
            break;
        case 1:
        {
            setSceneLights();
            std::vector<std::string> infoEntities;
            {
                Entity ent = MeshLoader::loadMesh(common::Definitions::m_defaultMeshesPath +
                                                  "/cube_with_material_no_emissive.obj");
                Entity entChild = g_coordinator.getComponent<Relationship>(ent)
                                      .first_child;  // we need a the component with the mesh
                auto &transfMeshEnt1 = g_coordinator.getComponent<Transform>(ent);
                transfMeshEnt1.translate(vec3_t(0, 0, -4));
                transfMeshEnt1.rotate(vec3_t(90, 0, 0));
                auto &mat = g_coordinator.getComponent<Material>(entChild);
                mat.shader = std::make_shared<Shader>(
                    common::Definitions::m_shadersFolderPath + "/texturedLightShader.vert",
                    common::Definitions::m_shadersFolderPath + "/texturedLightShader.frag");
            }
            {
                Entity entity = MeshLoader::loadMesh(common::Definitions::m_defaultMeshesPath +
                                                     "/wizardin.obj");
                auto &tranf = g_coordinator.getComponent<Transform>(entity);
                tranf.translate(vec3_t(-5, 0, 0));
            }

            for (auto &infoEntity : infoEntities)
            {
                Log::debug(infoEntity);
            }
            break;
        }
        case 2:
        {
            setSceneLights();
            // TODO(Pablo): These should be prefabs, we move them for now to a non visible place
            vec3_t prefabPosition = vec3_t(0, -200, 0);
            Entity entityWoodBox = MeshLoader::loadMesh(common::Definitions::m_defaultMeshesPath +
                                                        "/cube_with_diffuse.obj");
            Entity meshEntCubeWithEmissive = MeshLoader::loadMesh(
                common::Definitions::m_defaultMeshesPath + "/metalic_cube_with_material.obj");
            Entity meshEntCubeWithoutEmissive = MeshLoader::loadMesh(
                common::Definitions::m_defaultMeshesPath + "/cube_with_material_no_emissive.obj");
            g_coordinator.getComponent<Transform>(entityWoodBox).setLocalPosition(prefabPosition);
            g_coordinator.getComponent<Transform>(meshEntCubeWithEmissive)
                .setLocalPosition(prefabPosition);
            g_coordinator.getComponent<Transform>(meshEntCubeWithoutEmissive)
                .setLocalPosition(prefabPosition);
            // Use the prefabs to instantiate copies of them
            {
                Entity entity = g_coordinator.cloneEntity(entityWoodBox);
                auto &transf = g_coordinator.getComponent<Transform>(entity);
                transf.setLocalPosition(vec3_t(4, 0, 0));
                transf.setLocalRotation(vec3_t(90, 0, 0));
            }

            {
                Entity entity = g_coordinator.cloneEntity(entityWoodBox);
                auto &transformation = g_coordinator.getComponent<Transform>(entity);
                transformation.setLocalPosition(vec3_t(0, 0, -5));
                // Change the material (from the child since the parent doesn't have the geometry)
                Entity cubeGeomEnt = g_coordinator.getComponent<Relationship>(entity).first_child;
                setDefaultMaterial(cubeGeomEnt);
            }
            {
                Entity entity = g_coordinator.cloneEntity(meshEntCubeWithEmissive);
                auto &transf = g_coordinator.getComponent<Transform>(entity);
                transf.setLocalPosition(vec3_t(-8, 0, 0));
            }
            {
                Entity entity = g_coordinator.cloneEntity(meshEntCubeWithoutEmissive);
                auto &transformation = g_coordinator.getComponent<Transform>(entity);
                transformation.setLocalPosition(vec3_t(0, 0, 0));
                transformation.scaling(vec3_t(0.7, 0.7, 0.7));
                // Change the material.
                Entity cubeGeomEnt = g_coordinator.getComponent<Relationship>(entity).first_child;
                auto &mat = g_coordinator.getComponent<Material>(cubeGeomEnt);
                mat.shader = std::make_shared<Shader>(
                    common::Definitions::m_shadersFolderPath + "/texturedLightShader.vert",
                    common::Definitions::m_shadersFolderPath + "/texturedLightShader.frag");
                glm::vec3 cubePositions[] = {
                    glm::vec3(0.0F, 0.0F, 0.0F),    glm::vec3(2.0F, 5.0F, -15.0F),
                    glm::vec3(-1.5F, -2.2F, -2.5F), glm::vec3(-3.8F, -2.0F, -12.3F),
                    glm::vec3(2.4F, -0.4F, -3.5F),  glm::vec3(-1.7F, 3.0F, -7.5F),
                    glm::vec3(1.3F, -2.0F, -2.5F),  glm::vec3(1.5F, 2.0F, -2.5F),
                    glm::vec3(1.5F, 0.2F, -1.5F),   glm::vec3(-1.3F, 1.0F, -1.5F)};

                for (int i = 1; i < std::size(cubePositions); i++)
                {
                    Entity entCloned = g_coordinator.cloneEntity(entity);
                    auto &transformCloned = g_coordinator.getComponent<Transform>(entCloned);
                    float angle = 20.0F * static_cast<float>(i);
                    transformCloned.setLocalPosition(cubePositions[i]);
                    transformCloned.rotate(glm::radians(angle) * vec3_t(1.0F, 0.3F, 0.5F));
                }

                for (size_t i = 0; i < 10; i++)
                {
                    Entity ent = g_coordinator.cloneEntity(entity);
                    auto &transform = g_coordinator.getComponent<Transform>(ent);
                    transform.setLocalPosition(vec3_t(4, 0, 4 + static_cast<float>(i) * 2.5));
                }
            }
            {
                Entity entity = MeshLoader::loadMesh(common::Definitions::m_defaultMeshesPath +
                                                     "/wizardin.obj");
                auto &tranf = g_coordinator.getComponent<Transform>(entity);
                Log::debug("Transformation to entity " +
                           g_coordinator.getComponent<Metadata>(entity).name);
                tranf.translate(vec3_t(-5, 0, 0));

                for (int i = 1; i < 5; i++)
                {
                    Entity testEntCloned = g_coordinator.cloneEntity(entity);
                    auto &transformCloned = g_coordinator.getComponent<Transform>(testEntCloned);
                    transformCloned.translate(vec3_t(0, 0, i * 5));
                }
            }
            // Rotating object
            Entity entity =
                MeshLoader::loadMesh(common::Definitions::m_defaultMeshesPath + "/wizardin.obj");
            auto &transf = g_coordinator.getComponent<Transform>(entity);
            transf.translate(vec3_t(0, 4, 2));
            transf.scaling(vec3_t(0.2, 0.2, 0.2));
            SimpleAnimation simpleAnimation;
            simpleAnimation.rotationPerSecond = vec3_t(0, 45, 0);
            simpleAnimation.transformationStartingPos = transf.getLocalPosition();
            simpleAnimation.transformationTargetPos = transf.getLocalPosition() + vec3_t(4, 0, 0);
            simpleAnimation.transformationSpeed = 0.7;
            g_coordinator.addComponent<SimpleAnimation>(entity, simpleAnimation);
        }
        break;
        default:
            Log::error("Error! the scene id " + std::to_string(a_sceneId) + " is not valid");
            break;
    }
}

void SandboxLayer::setSceneLights()
{
    // TODO(Pablo): all scene lights must be set since the number is hardcoded currently in the
    // shader, pending to be solved
    vec3_t pointLightPositions[] = {vec3_t(0.7F, 0.2F, 2.0F), vec3_t(2.3F, -3.3F, -4.0F),
                                    vec3_t(-4.0F, 2.0F, -12.0F), vec3_t(0.0F, 0.0F, -3.0F)};

    vec3_t pointLightColors[] = {vec3_t(0.7, 0.7, 0.7), vec3_t(0.1, 0.7, 0.4),
                                 vec3_t(0.3, 0.1, 0.7), vec3_t(0.8, 0.1, 0.2)};
    createDirectionalLight(vec3_t(0, -1, 0), vec3_t(0.2, 0.2, 0.2), vec3_t(0.2, 0.2, 0.2),
                           vec3_t(1, 1, 1));
    for (size_t i = 0; i < 4; i++)
    {
        createPointLight(pointLightPositions[i], color3_t(0.2, 0.2, 0.2), pointLightColors[i],
                         color3_t(1.F, 1.F, 1.F), 0.07F, 0.017F);
    }
    auto spotlightEntity = createSpotLight(
        vec3_t(0, 3, 0), vec3_t(0, -1, 0), color3_t(0.0, 0.2, 0.0), color3_t(0.0, 0.7, 0.0),
        color3_t(0, 1, 0), 0.07, 0.017, static_cast<float>(glm::cos(glm::radians(12.5))),
        static_cast<float>(glm::cos(glm::radians(17.5))));

    SimpleAnimation simpleAnimation;
    auto transf = g_coordinator.getComponent<Transform>(spotlightEntity);
    simpleAnimation.transformationStartingPos = transf.getLocalPosition() + vec3_t(4, 1, 0);
    ;
    simpleAnimation.transformationTargetPos = transf.getLocalPosition() + vec3_t(-4, 1, 0);
    simpleAnimation.transformationSpeed = 0.5;
    g_coordinator.addComponent<SimpleAnimation>(spotlightEntity, simpleAnimation);
}

void SandboxLayer::setDefaultMaterial(Entity a_entity)
{
    auto &mat = g_coordinator.getComponent<Material>(a_entity);
    mat.ambient = color3_t(1.0F, 0.5F, 0.31F);
    mat.diffuse = color3_t(1.0F, 0.5F, 0.31F);
    mat.specular = color3_t(0.5F, 0.5F, 0.5F);
    mat.shininess = 32.0F;
    mat.textures.clear();
}

Entity SandboxLayer::createDirectionalLight(vec3_t a_direction, color3_t a_ambiental,
                                            color3_t a_diffuse, color3_t a_specular)
{
    if (m_prefabLightMeshEnt == NULL_ENTITY)
    {
        createLightMeshPrefab();
    }
    Entity lightEntity = g_coordinator.createEntity();
    DirectionalLight dirLightComponent;
    dirLightComponent.ambient = a_ambiental;
    dirLightComponent.diffuse = a_diffuse;
    dirLightComponent.specular = a_specular;
    size_t numDirLights = m_directionalLights.size();
    g_coordinator.getComponent<Metadata>(lightEntity).name =
        "directionalLight_" + std::to_string(numDirLights);
    g_coordinator.addComponent<DirectionalLight>(lightEntity, dirLightComponent);
    addLightCommonComponents<DirectionalLight>(
        lightEntity, vec3_t(0, 8 - numDirLights, 0),
        a_direction);  // some position for debugging purposes
    m_directionalLights.emplace_back(lightEntity);
    return lightEntity;
}

Entity SandboxLayer::createPointLight(vec3_t a_position, color3_t a_ambiental, color3_t a_diffuse,
                                      color3_t a_specular, float a_linearAttenuation,
                                      float a_quadraticAttenuation)
{
    // Attenuation values in Ogre3d's wiki
    // http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation

    if (m_prefabLightMeshEnt == NULL_ENTITY)
    {
        createLightMeshPrefab();
    }
    Entity lightEntity = g_coordinator.createEntity();
    PointLight lightComponent;
    lightComponent.ambient = a_ambiental;
    lightComponent.diffuse = a_diffuse;
    lightComponent.specular = a_specular;
    lightComponent.linear = a_linearAttenuation;
    lightComponent.quadratic = a_quadraticAttenuation;
    size_t numLights = m_pointLights.size();
    g_coordinator.getComponent<Metadata>(lightEntity).name =
        "pointLight_" + std::to_string(numLights);
    g_coordinator.addComponent<PointLight>(lightEntity, lightComponent);
    addLightCommonComponents<PointLight>(lightEntity, a_position, vec3_t(0, 0, 0));
    m_pointLights.emplace_back(lightEntity);
    return lightEntity;
}

Entity SandboxLayer::createSpotLight(vec3_t a_position, color3_t a_direction, color3_t a_ambiental,
                                     color3_t a_diffuse, color3_t a_specular,
                                     float a_linearAttenuation, float a_quadraticAttenuation,
                                     float a_cutoff, float a_outerCutoff)
{
    // Attenuation values in Ogre3d's wiki
    // http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation There will be the light
    // with the component and a child with the geometry. Is to avoid the mesh transform (e.g scale)
    // to affect the light transform (could be solved also by creating a mesh without scale)

    if (m_prefabLightMeshEnt == NULL_ENTITY)
    {
        createLightMeshPrefab();
    }
    Entity lightEntity = g_coordinator.createEntity();
    SpotLight lightComponent;
    lightComponent.ambient = a_ambiental;
    lightComponent.diffuse = a_diffuse;
    lightComponent.specular = a_specular;
    lightComponent.linear = a_linearAttenuation;
    lightComponent.quadratic = a_quadraticAttenuation;
    lightComponent.cutOff = a_cutoff;
    lightComponent.outerCutoff = a_outerCutoff;
    size_t numLights = m_spotLights.size();
    g_coordinator.getComponent<Metadata>(lightEntity).name =
        "spotLight_" + std::to_string(numLights);
    g_coordinator.addComponent<SpotLight>(lightEntity, lightComponent);
    addLightCommonComponents<SpotLight>(lightEntity, a_position, a_direction);
    m_spotLights.emplace_back(lightEntity);
    return lightEntity;
}

template <typename T>
void SandboxLayer::addLightCommonComponents(Entity a_entity, vec3_t a_pos, vec3_t a_direction)
{
    Entity lightMeshEntity = g_coordinator.cloneEntity(m_prefabLightMeshEnt);
    g_coordinator.setParentEntity(lightMeshEntity, a_entity);
    Transform transform;
    transform.setLocalRotation(a_direction);
    transform.setLocalPosition(a_pos);
    g_coordinator.addComponent<Transform>(a_entity, transform);
    g_coordinator.getComponent<Transform>(lightMeshEntity).setLocalPosition(vec3_t(0, 0, 0));

    // TODO(Pablo): in the current state of the ECS a component can not be shared
    auto &material = g_coordinator.getComponent<Material>(lightMeshEntity);
    material.shader = std::make_shared<Shader>(
        common::Definitions::m_shadersFolderPath + "/simpleLightShader.vert",
        common::Definitions::m_shadersFolderPath + "/simpleLampShader.frag");
    material.ambient = g_coordinator.getComponent<T>(a_entity).ambient;
    material.diffuse = g_coordinator.getComponent<T>(a_entity).diffuse;
    material.specular = g_coordinator.getComponent<T>(a_entity).specular;
    material.shininess = 64;
}

void SandboxLayer::createLightMeshPrefab()
{
    Entity lightEntParent =
        MeshLoader::loadMesh(common::Definitions::m_defaultMeshesPath + "/cube_with_diffuse.obj");
    auto &relLightEntParent = g_coordinator.getComponent<Relationship>(lightEntParent);
    Entity lightEnt = relLightEntParent.first_child;
    g_coordinator.setEntityAsRootEntity(lightEnt);  // we only need the component with the mesh
    auto &transf = g_coordinator.getComponent<Transform>(lightEnt);
    // TODO(Pablo): prefabs should not be visible, for now we move them to an unused position.
    transf.setLocalPosition(vec3_t(0, -200, 0));
    transf.scaling(vec3_t(0.1, 0.1, 0.1));
    auto &material = g_coordinator.getComponent<Material>(lightEnt);
    material.shader = std::make_shared<Shader>(
        common::Definitions::m_shadersFolderPath + "/simpleLightShader.vert",
        common::Definitions::m_shadersFolderPath + "/simpleLampShader.frag");
    material.ambient = vec3_t(0.2, 0.2, 0.2);
    material.diffuse = vec3_t(0.7, 0.7, 0.7);
    material.specular = vec3_t(1, 1, 1);
    material.shininess = 64;
    material.textures.clear();
    g_coordinator.getComponent<Metadata>(lightEnt).name = "meshLight_prefab";
    m_prefabLightMeshEnt = lightEnt;
}