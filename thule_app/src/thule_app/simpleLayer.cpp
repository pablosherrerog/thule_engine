#include "thule_app/simpleLayer.hpp"

#include <thule_engine/common/pathsDefinitions.hpp>
#include <thule_engine/components/metadata.hpp>
#include <thule_engine/core/ecs/coordinator.hpp>
#include <thule_engine/core/layer.hpp>
#include <thule_engine/geom/meshLoader.hpp>

#include "thule_engine/components/camera.hpp"
#include "thule_engine/components/directionalLight.hpp"
#include "thule_engine/components/geometry.hpp"
#include "thule_engine/components/material.hpp"
#include "thule_engine/components/metadata.hpp"
#include "thule_engine/components/pointLight.hpp"
#include "thule_engine/components/relationship.hpp"
#include "thule_engine/components/renderable.hpp"
#include "thule_engine/components/spotLight.hpp"
#include "thule_engine/components/transform.hpp"
extern Coordinator g_coordinator;

void SimpleLayer::onAttach()
{
    Entity n1 = g_coordinator.createEntity("n1");
    Entity n2 = g_coordinator.createEntity(n1, "n2");
    Entity n3 = g_coordinator.createEntity(n1, "n3");
    Entity n4 = g_coordinator.createEntity(n2, "n4");
    Entity n5 = g_coordinator.createEntity(n2, "n5");
    Entity n6 = g_coordinator.createEntity("n6");
    g_coordinator.setParentEntity(n6, n5);
    // Entity entity =
    // MeshLoader::loadMesh(common::Definitions::m_defaultMeshesPath + "/wizardin.obj");
}