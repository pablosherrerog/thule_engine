#pragma once
#include <memory>
#include <vector>

#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/core/layer.hpp"
#include "thule_engine/geom/geomDef.hpp"
#include "thule_engine/systems/simpleAnimSystem.hpp"

class TransformationUpdaterSystem;
class RenderSystem;
class CameraControlSystem;
class SimpleAnimSystem;
class SandboxLayer : public Layer
{
   public:
    SandboxLayer();
    ~SandboxLayer() override = default;
    void onAttach() override;
    void onDetach() override{};
    void onUpdate(float a_timeStepSec) override;
    // Having here the main camera not sure it's a good idea, should be in editor layer or similar.
    static Entity getEditorCamera() { return m_mainCameraEnt; };

   private:
    void loadPredefinedScene(int a_sceneId);
    void initScene();

    static void testQuad();
    void setSceneLights() const;
    static void setDefaultMaterial(Entity a_entity);
    std::vector<Entity> m_directionalLights;
    std::vector<Entity> m_pointLights;
    std::vector<Entity> m_spotLights;
    static Entity m_mainCameraEnt;
    Entity m_prefabLightMeshEnt = NULL_ENTITY;
    Entity createDirectionalLight(vec3_t a_direction, vec3_t a_ambiental, vec3_t a_diffuse,
                                  vec3_t a_specular);
    Entity createPointLight(vec3_t a_position, vec3_t a_ambiental, vec3_t a_diffuse,
                            vec3_t a_specular, float a_linearAttenuation,
                            float a_quadraticAttenuation);
    Entity createSpotLight(vec3_t a_position, vec3_t a_direction, vec3_t a_ambiental,
                           vec3_t a_diffuse, vec3_t a_specular, float a_linearAttenuation,
                           float a_quadraticAttenuation, float a_cutoff, float a_outerCutoff);
    std::shared_ptr<RenderSystem> m_renderSystemPtr;
    std::shared_ptr<CameraControlSystem> m_cameraControlSystemPtr;
    std::shared_ptr<TransformationUpdaterSystem> m_transformationUpdaterSystemPtr;
    std::shared_ptr<SimpleAnimSystem> m_simpleAnimationSystemPtr;
    template <typename T>
    void addLightCommonComponents(Entity a_entity, vec3_t a_pos, vec3_t a_direction);
    void createLightMeshPrefab();
    void setSceneLights();
};