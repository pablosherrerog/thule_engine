#pragma once
#include <thule_engine/core/layer.hpp>
class SimpleLayer : public Layer
{
   public:
    SimpleLayer() : Layer("Simple layer"){};
    ~SimpleLayer() override = default;
    void onUpdate(float a_timeStepSec) override{};
    void onAttach() override;
    void onDetach() override{};
};