#pragma once
#include <map>
#include <string>

class ImGuiWindow;
class GuiPanel
{
   public:
    virtual void onAttach() = 0;
    virtual void onUpdate() = 0;
    virtual void processInput(){};  // override it when it's used.
    // If the mouse is pressed then it will wrap the mouse inside the window boundaries.
    void wrapMouseIntoPanelWhenPressed();
    void disableInput() { m_inputEnabled = false; };
    void enableInput() { m_inputEnabled = true; };
    const std::string& getPanelName() const { return m_panelName; };

   protected:
    std::string m_panelName;
    bool m_viewportMouseDragging = false;
    bool m_viewportHovered = false;
    bool m_inputEnabled = true;  // If other panel is being focused (e.g. dragging the mouse button)
                                 // the panel should not receive input.
};