#pragma once

#include <map>
#include <memory>
#include <thule_engine/core/layer.hpp>

#include "thule_app/gui/editor/sceneHierarchyPanel.hpp"
#include "thule_app/gui/guiPanel.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/render/framebuffer.hpp"

class ImGuiLayer : public Layer
{
   public:
    ImGuiLayer();
    ~ImGuiLayer() override = default;

    void onUpdate(float a_dt) override;
    void onAttach() override;
    void onDetach() override;
    void onImguiRender();
    // We use begin and end functions to
    void begin();
    void end(float a_dt);
    static void setDarkThemeColors();

   private:
    Entity m_editorCameraEnt = NULL_ENTITY;
    SceneHierarchyPanel m_sceneHierarchyPanel;
    std::map<std::string, std::unique_ptr<GuiPanel>> m_guiPanels;
};