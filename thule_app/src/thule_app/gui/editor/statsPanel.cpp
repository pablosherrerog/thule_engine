#include "thule_app/gui/editor/statsPanel.hpp"

#include <imgui/imgui.h>

#include "thule_app/gui/guiPanel.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/core/events/engineEvents.hpp"

void StatsPanel::onAttach()
{
    auto statsUpdated = [&](const Event& a_event)
    {
        auto fpsEvent = static_cast<const engine_events::FPSUpdated&>(a_event);
        m_currentFPS = fpsEvent.getFPS();
    };
    Dispatcher::subscribe(event_types::EventType::k_fpsUpdated, statsUpdated);
}
void StatsPanel::onUpdate()
{
    ImGui::Begin("Stats");
    ImGui::Text("FPS: %f ", m_currentFPS);
    // ImGui::Text("Hovered Entity: %s", "To be implemented");
    ImGui::End();
}
