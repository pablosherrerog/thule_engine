#pragma once
#include <memory>

#include "thule_app/gui/guiPanel.hpp"

class StatsPanel : public GuiPanel
{
   public:
    StatsPanel() { m_panelName = getPanelName(); }
    void onUpdate() override;
    void onAttach() override;
    static constexpr const char* getPanelName() { return "Stats panel"; };

   private:
    float m_currentFPS{0};
};