
#pragma once
#include <memory>

#include "thule_app/gui/guiPanel.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/render/framebuffer.hpp"
#include "thule_engine/render/textures/texture.hpp"

class SceneViewport : public GuiPanel
{
   public:
    SceneViewport() { m_panelName = getPanelName(); }
    void onUpdate() override;
    void onAttach() override;
    static constexpr const char* getPanelName() { return "Viewport"; };

   private:
    void onImguiRender();
    void processInput() override;
    std::shared_ptr<Texture> m_checkerboardTexturePtr;
    std::shared_ptr<Framebuffer> m_framebufferPtr = nullptr;
    Entity m_editorCameraEnt = NULL_ENTITY;
};