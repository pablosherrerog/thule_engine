#include "thule_app/gui/editor/sceneHierarchyPanel.hpp"

#include <imgui/imgui.h>
#include <imgui/imgui_stdlib.h>
#include <vcruntime.h>

#include <memory>
#include <set>
#include <string>
#include <thule_app/gui/guiComponent.hpp>
#include <thule_engine/common/log.hpp>
#include <thule_engine/components/metadata.hpp>
#include <thule_engine/components/transform.hpp>
#include <thule_engine/core/commonCoreUtils.hpp>
#include <thule_engine/core/ecs/coordinator.hpp>
#include <thule_engine/core/ecs/typesECS.hpp>
#include <thule_engine/core/events/appEvents.hpp>
#include <thule_engine/core/events/engineEvents.hpp>
#include <thule_engine/core/events/event.hpp>
#include <thule_engine/core/events/keyboardEvents.hpp>
#include <thule_engine/core/metaprograming.hpp>

#include "imgui/imgui_internal.h"
#include "thule_app/gui/imGuiLayer.hpp"
#include "thule_app/gui/imGuiUtils.hpp"
#include "thule_engine/common/exceptionUtils.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/core/application.hpp"

extern Coordinator g_coordinator;

static void HelpMarker(const char *desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0F);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}
void SceneHierarchyPanel::onAttach()
{
    // Doubt: Not sure if this event subscription could be in another place
    auto deselectEntityLambda = [&](const Event &a_event)
    {
        auto deselectEntityEvent = static_cast<const app_events::DeselectEntity &>(a_event);
        this->deselectEntity(deselectEntityEvent.getDeselectedEntity());
    };
    Dispatcher::subscribe(event_types::EventType::k_entityDeselected, deselectEntityLambda);
    auto deleteEntityOnDelKey = [&](const Event &a_event)
    {
        auto keyDownEvent = static_cast<const KeyboardEvents::KeyDown &>(a_event);
        if (keyDownEvent.getKeyDown() == Key::Delete)
        {
            if (m_selectedEntity != NULL_ENTITY)
            {
                Dispatcher::post(engine_events::DeleteEntityEvent(m_selectedEntity));
            }
        }
    };
    Dispatcher::subscribe(event_types::EventType::k_keyboard, deleteEntityOnDelKey);
}
void SceneHierarchyPanel::processInput()
{
    // In some cases the scene hierarchy has some problems with the placement of the imgui area to
    // be wrapped. So it is deactivated for now. wrapMouseIntoPanelWhenPressed();
}
void SceneHierarchyPanel::onUpdate()
{
    PROFILER_SCOPED;
    ImGuiWindowFlags windowFlags = 0;
    // Disable the imgui input in the window if needed (e.g. if other window is
    // on focus and has disabled this guiPanel input)
    if (!m_inputEnabled)
    {
        windowFlags |= ImGuiWindowFlags_NoMouseInputs;
    }

    ImGui::Begin(getPanelName(), nullptr, windowFlags);
    processInput();
    onImguiRender();
    ImGui::End();
    // TODO(Pablo): the properties should go to another gui panel class.
    // Properties window
    ImGui::Begin("Properties");
    if (m_selectedEntity != NULL_ENTITY && !g_coordinator.entityExists(m_selectedEntity))
    {
        // It is an error since it implies some problem with the engine.
        THULE_ASSERTION_ERROR(
            "Selected entity " + std::to_string(m_selectedEntity) +
            " doesn't exist. Maybe it was removed but not deselected from the gui")
    }
    drawComponents(m_selectedEntity);
    ImGui::End();
}
void SceneHierarchyPanel::onImguiRender()
{
    static ImGuiTreeNodeFlags base_flags = ImGuiTreeNodeFlags_SpanAvailWidth |
                                           ImGuiTreeNodeFlags_OpenOnArrow |
                                           ImGuiTreeNodeFlags_OpenOnDoubleClick;
    if (ImGui::IsMouseDown(0) && ImGui::IsWindowHovered())
    {
        // Log::info("MOusse down in scene hierarhy panel");
    }
    if (ImGui::TreeNodeEx("Scene", base_flags | ImGuiTreeNodeFlags_DefaultOpen))
    {
        if (ImGui::BeginPopupContextItem())
        {
            if (ImGui::MenuItem("Create Entity"))
            {
                g_coordinator.createEntity("Empty Entity");
            }
            ImGui::EndPopup();
        }
        ImGui::SameLine();
        HelpMarker(
            "Example tree.\n"
            "Click to select, CTRL+Click to toggle, click on arrows or double-click to open.");
        // Pass function to itself to allow recursive lambda (Recursive lambdas require C++14)
        auto createHierarchyEntry = [&](auto &&a_createHierarchyEntry, Entity a_entity) -> void
        {
            if (a_entity == NULL_ENTITY)
            {
                return;
            }
            ImGuiTreeNodeFlags nodeFlags = base_flags;
            auto relationship = g_coordinator.getComponent<Relationship>(a_entity);
            auto metadata = g_coordinator.getComponent<Metadata>(a_entity);
            Entity child = relationship.first_child;
            if (child == NULL_ENTITY)
            {
                nodeFlags |= ImGuiTreeNodeFlags_Leaf;
            }
            // Draw the entity as an entry (and the child if the parent is open)
            bool open =
                ImGui::TreeNodeEx((void *)(intptr_t)a_entity, nodeFlags, metadata.name.c_str());
            if (ImGui::BeginPopupContextItem())
            {
                if (ImGui::MenuItem(std::string("Delete entity " + metadata.name).c_str()))
                {
                    Dispatcher::post<engine_events::DeleteEntityEvent>(
                        engine_events::DeleteEntityEvent(a_entity));
                }

                if (ImGui::MenuItem("Create Entity"))
                {
                    g_coordinator.createEntity(a_entity, "Empty Entity");
                }
                ImGui::EndPopup();
            }
            if (ImGui::IsItemHovered()) ImGui::SetTooltip("Right-click to open popup");
            if (ImGui::IsItemClicked())
            {
                Log::info("Item " + metadata.name + " clicked");
                m_selectedEntity = a_entity;
            }

            if (open)
            {
                a_createHierarchyEntry(a_createHierarchyEntry, child);
                ImGui::TreePop();
            }
            Entity sibling = g_coordinator.getComponent<Relationship>(a_entity).next_sibling;
            a_createHierarchyEntry(a_createHierarchyEntry, sibling);
        };
        // This works because all entities contain the needed components metadata and
        // relationship.
        for (Entity rootEntity : g_coordinator.getRootEntities())
        {
            createHierarchyEntry(createHierarchyEntry, rootEntity);
        }
        // Right-click on blank space
        if (ImGui::BeginPopupContextWindow(0, 1, false))
        {
            if (ImGui::MenuItem("Create Empty Entity"))
            {
                g_coordinator.createEntity("Empty Entity");
            }

            ImGui::EndPopup();
        }

        ImGui::TreePop();
    }
}

// Extracted from SceneHierarchyPanel.cpp Hazel https://github.com/TheCherno/Hazel
template <typename T>
static void drawComponentTab(Entity a_entity)
{
    const ImGuiTreeNodeFlags treeNodeFlags =
        ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_Framed |
        ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_AllowItemOverlap |
        ImGuiTreeNodeFlags_FramePadding;
    if (g_coordinator.hasComponent<T>(a_entity))
    {
        ImVec2 contentRegionAvailable = ImGui::GetContentRegionAvail();

        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2{4, 4});
        float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0F;
        ImGui::Separator();
        std::string name = CommonCoreUtils::getComponentName<T>();
        bool open =
            ImGui::TreeNodeEx((void *)typeid(T).hash_code(), treeNodeFlags, "%s", name.c_str());
        ImGui::PopStyleVar();
        ImGui::SameLine(contentRegionAvailable.x - lineHeight * 0.5F);
        if (ImGui::Button("+", ImVec2{lineHeight, lineHeight}))
        {
            ImGui::OpenPopup("ComponentSettings");
        }
        bool removeComponent = false;
        if (ImGui::BeginPopup("ComponentSettings"))
        {
            if (ImGui::MenuItem("Remove component"))
            {
                removeComponent = true;
            }
            if (ImGui::MenuItem("Reset component"))
            {
                g_coordinator.resetComponent<T>(a_entity);
            }

            ImGui::EndPopup();
        }

        if (open)
        {
            GUIComponent<T>::drawGUIComponent(a_entity);
            ImGui::TreePop();
        }
        if (removeComponent)
        {
            // Remove component at the end when the component is not used anymore.
            g_coordinator.removeComponent<T>(a_entity);
        }
    }
}
// Call it with addComponentsLogic<0>
template <int N>
void drawComponentsLogic(Entity a_entity)
{
    using ComponentType = typename mp::NthTypeTypeList<N, ComponentList>::type;
    std::string name = CommonCoreUtils::getComponentName<ComponentType>();
    // Both Metadata and Relationship are special cases
    if (g_coordinator.hasComponent<ComponentType>(a_entity) &&
        !mp::IsSame<ComponentType, Metadata>::value &&
        !mp::IsSame<ComponentType, Relationship>::value)
    {
        drawComponentTab<ComponentType>(a_entity);
    }
    if constexpr (N + 1 < ComponentList::size())
    {
        drawComponentsLogic<(N + 1)>(a_entity);
    }
}

void SceneHierarchyPanel::drawComponents(Entity a_entity)
{
    if (a_entity == NULL_ENTITY)
    {
        return;
    }
    auto &metadata = g_coordinator.getComponent<Metadata>(a_entity);
    // The ## is is to indicate that the rest of the text is a tag, if not it would display it.
    ImGui::InputText("##Name", &metadata.name);
    ImGui::SameLine();
    ImGui::Text("Entity %s", std::to_string(a_entity).c_str());
    ImGui::SameLine();
    // ImGui::InputText("##Name", &metadata.name);
    // ImGui::PushItemWidth(-1);

    if (ImGui::Button("Add Component"))
    {
        ImGui::OpenPopup("AddComponent");
    }
    if (ImGui::BeginPopup("AddComponent"))
    {
        addComponentsLogic<0>(a_entity);
        ImGui::EndPopup();
    }
    drawComponentsLogic<0>(a_entity);
}
void SceneHierarchyPanel::deselectEntity(Entity a_entity)
{
    if (m_selectedEntity == a_entity)
    {
        m_selectedEntity = NULL_ENTITY;
    }
}
