// Part of this code was extracted from https://github.com/TheCherno/Hazel
// and https://github.com/ocornut/imgui/blob/docking/examples/example_glfw_opengl3/main.cpp
// https://www.youtube.com/watch?v=lZuje-3iyVE&list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT&index=24&t=510s
#include "thule_app/gui/editor/sceneViewport.hpp"

#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <memory>
#include <string>

#include "thule_app/gui/imGuiUtils.hpp"
#include "thule_app/sandboxLayer.hpp"
#include "thule_engine/common/exceptionUtils.hpp"
#include "thule_engine/common/log.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/components/camera.hpp"
#include "thule_engine/core/application.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/core/events/appEvents.hpp"
#include "thule_engine/core/events/engineEvents.hpp"
#include "thule_engine/core/events/keyboardEvents.hpp"
#include "thule_engine/core/input/keyCodes.hpp"
#include "thule_engine/geom/geomDef.hpp"
#include "thule_engine/render/framebuffer.hpp"
#include "thule_engine/render/textures/texture.hpp"
#include "thule_engine/render/textures/textureManager.hpp"

extern Coordinator g_coordinator;

void SceneViewport::onAttach()
{
    m_checkerboardTexturePtr = TextureManager::create("checkerboard.png", TextureType::diffuse);

    std::queue<engine_events::FramebufferInitialization> postedEvents =
        Dispatcher::getPostedEvent<engine_events::FramebufferInitialization>();
    if (postedEvents.empty())
    {
        THULE_ASSERTION_ERROR("Error! the framebuffer should be initialized before the gui.")
    }
    else if (postedEvents.size() != 1)
    {
        THULE_ASSERTION_ERROR(
            "Error! there are more than 1 frame buffer initialization, should be only 1.")
    }
    else
    {
        m_framebufferPtr = postedEvents.front().getFramebufferPtr();
    }
    m_editorCameraEnt = SandboxLayer::getEditorCamera();
    if (m_editorCameraEnt == NULL_ENTITY)
    {
        THULE_EXCEPTION("Error! the editor entity camera is null");
    }
}

void SceneViewport::processInput()
{
    bool hideCursorOnDrag = false;
    wrapMouseIntoPanelWhenPressed();
    auto& editorCamera = g_coordinator.getComponent<Camera>(m_editorCameraEnt);
    // Activate the camera input if the second click is pressed.
    // TODO(Pablo): replace these mouse conditionals with mouse events. Cleaner
    //  and easier to refactor.
    if (ImGui::IsMouseDown(1) && m_viewportMouseDragging)
    {
        editorCamera.enabledCameraInput = true;
        if (hideCursorOnDrag)
        {
            Application::getApp().getWindow().lockCursorToWindow();
        }
    }
    if (!ImGui::IsMouseDown(1))
    {
        editorCamera.enabledCameraInput = false;
        if (hideCursorOnDrag)
        {
            Application::getApp().getWindow().unlockCursorToWindow();
        }
    }
}

void SceneViewport::onUpdate()
{
    PROFILER_SCOPED;
    ImGuiWindowFlags windowFlags = 0;
    // Disable the imgui input in the window if needed (e.g. if other window is
    // on focus and has disabled this guiPanel input)
    if (!m_inputEnabled)
    {
        windowFlags |= ImGuiWindowFlags_NoMouseInputs;
    }

    ImGui::Begin(getPanelName(), nullptr, windowFlags);
    processInput();
    onImguiRender();
    ImGui::End();
}

// In this function it's done the imgui rendering. Will be placed between the functions
// begin and end.
void SceneViewport::onImguiRender()
{
    // Remove padding from viewport.
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2{0, 0});
    ImVec2 viewportPanelSize = ImGui::GetContentRegionAvail();
    uint32_t colorBufferID = 12345;
    if (m_framebufferPtr == nullptr)
    {
        colorBufferID = m_checkerboardTexturePtr->getTextureID();
    }
    else
    {
        colorBufferID = m_framebufferPtr->getTextureColorID();
        FramebufferSpecification oldView = m_framebufferPtr->getSpecification();
        // If the viewport changed, change the size of the framebuffer
        auto newWidth = static_cast<uint32_t>(viewportPanelSize.x);
        auto newHeight = static_cast<uint32_t>(viewportPanelSize.y);
        if (oldView.width != newWidth || oldView.height != newHeight)
        {
            m_framebufferPtr->resize(viewportPanelSize.x, viewportPanelSize.y);
            // We need to updates cameras aspect ratio so we launch an event to indicate that the
            // scene viewport changed.
            Dispatcher::broadcast(app_events::SceneViewportResize(newWidth, newHeight));
        }
    }
    // Note: certain calls from ImGui like ImGui::Text should be passed before the Image call to
    // be able to show them.

    ImGui::Image((void*)(colorBufferID), ImVec2{viewportPanelSize.x, viewportPanelSize.y},
                 ImVec2{0, 1}, ImVec2{1, 0});
    // ImGuiUtils::highlightContentRegion();
    ImGui::PopStyleVar();  // To avoid other panels to have a padding of 0
}
