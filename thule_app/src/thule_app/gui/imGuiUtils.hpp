#pragma once
#include <imgui/imgui.h>

#include <tuple>

#include "thule_engine/geom/geomDef.hpp"

class ImGuiUtils
{
   public:
    static void wrapMouseAroundContentRegionFromMouseInput();
    static void highlightContentRegion();
    static void setDarkThemeColors();

   private:
    static std::tuple<ImVec2, ImVec2> getAbsouluteMinMaxRegionBoundaries();
    static ImVec2 wrapMouseAroundContentRegion(ImVec2 a_windowMaxBoundaries,
                                               ImVec2 a_windowMinBoundaries, ImVec2 a_windowSize,
                                               ImVec2 a_currentPosInWindowCoordinates,
                                               ImVec2 a_currentPos);

    static void updateMouseWrapEvent(vec2_t a_boundaryExitPos, vec2_t a_boundaryEntryPos);
};