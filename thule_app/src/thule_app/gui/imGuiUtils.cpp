#include "thule_app/gui/imGuiUtils.hpp"

#include <GLFW/glfw3.h>
#include <imgui/imgui.h>

#include <glm/glm.hpp>
#include <string>
#include <tuple>

#include "imgui/imgui_internal.h"
#include "thule_engine/common/log.hpp"
#include "thule_engine/core/application.hpp"
#include "thule_engine/core/events/appEvents.hpp"
#include "thule_engine/core/events/event.hpp"
#include "thule_engine/core/input/input.hpp"
#include "thule_engine/core/window.hpp"
#include "thule_engine/geom/geomDef.hpp"

/*Removes the last mouse info event and creates a new one with the mouse displaced from one
window boundary to another*/
void ImGuiUtils::updateMouseWrapEvent(vec2_t a_boundaryExitPos, vec2_t a_boundaryEntryPos)
{
    auto events = Dispatcher::getPostedEvent<app_events::MouseDisplacementWindowEvent>();
    if (!events.empty())
    {
        // Take the event and calculate the mouse displacement.
        app_events::MouseDisplacementWindowEvent mouseEvent = events.back();
        mouseEvent.resetLastMousePositionByBoundary(a_boundaryExitPos, a_boundaryEntryPos);
        Dispatcher::post<app_events::MouseDisplacementWindowEvent>(mouseEvent);
    }
}
/*Returns the mouse position, will displace the position to be contained between the boundaries.

It creates the necessary events to inform the rest of the systems about the mousedisplacement.

Args:
   a_windowMaxBoundaries: from IMGUI, in window coordinates (adds the window position)
   a_windowMinBoundaries: from IMGUI, in window coordinates (adds the window position)
   a_windowSize: from IMGUI, region width and height
   a_currentPosInWindowCoordinates: from IMGUI, in window coordinates (adds the window position)
   a_currentPos: is the mouse position given by glfw.

Returns the current position in current pos format format (glfw) displaced to the boundary in
case it was outside the boundaries.
*/
ImVec2 ImGuiUtils::wrapMouseAroundContentRegion(ImVec2 a_windowMaxBoundaries,
                                                ImVec2 a_windowMinBoundaries, ImVec2 a_windowSize,
                                                ImVec2 a_currentPosInWindowCoordinates,
                                                ImVec2 a_currentPos)
{
    int offset = 1;  // Add a small offset to avoid moving exactly to the boundary.
    float width = a_windowSize.x;
    float height = a_windowSize.y;
    if (a_currentPosInWindowCoordinates.x <= a_windowMinBoundaries.x)  // Left boundary
    {
        float distFromLimit = std::abs(a_currentPosInWindowCoordinates.x - a_windowMinBoundaries.x);
        float boundaryExitPos = a_currentPos.x + distFromLimit;
        float boundaryEntryPos = boundaryExitPos + width;
        updateMouseWrapEvent(vec2_t(boundaryExitPos, 0), vec2_t(boundaryEntryPos, 0));
        return ImVec2(boundaryEntryPos - offset, a_currentPos.y);
    }
    else if (a_currentPosInWindowCoordinates.x >= a_windowMaxBoundaries.x)  // Right boundary
    {
        float distFromLimit = std::abs(a_currentPosInWindowCoordinates.x - a_windowMaxBoundaries.x);
        float boundaryExitPos = a_currentPos.x - distFromLimit;
        float boundaryEntryPos = boundaryExitPos - width;
        updateMouseWrapEvent(vec2_t(boundaryExitPos, 0), vec2_t(boundaryEntryPos, 0));
        return ImVec2(boundaryEntryPos + offset, a_currentPos.y);
    }
    if (a_currentPosInWindowCoordinates.y <= a_windowMinBoundaries.y)  // Top boundary
    {
        float distFromLimit = std::abs(a_currentPosInWindowCoordinates.y - a_windowMinBoundaries.y);
        float boundaryExitPos = a_currentPos.y + distFromLimit;
        float boundaryEntryPos = boundaryExitPos + height;
        updateMouseWrapEvent(vec2_t(0, boundaryExitPos), vec2_t(0, boundaryEntryPos));

        return ImVec2(a_currentPos.x, boundaryEntryPos - offset);
    }
    else if (a_currentPosInWindowCoordinates.y >= a_windowMaxBoundaries.y)  // Bottom boundary
    {
        float distFromLimit = std::abs(a_currentPosInWindowCoordinates.y - a_windowMaxBoundaries.y);
        float boundaryExitPos = a_currentPos.y - distFromLimit;
        float boundaryEntryPos = boundaryExitPos - height;
        updateMouseWrapEvent(vec2_t(0, boundaryExitPos), vec2_t(0, boundaryEntryPos));
        return ImVec2(a_currentPos.x, boundaryEntryPos + offset);

        // Alternative code to move the mouse with imgui.
        // ImGuiIO& io = ImGui::GetIO();
        // io.WantSetMousePos = true;
        // io.MousePos.y = vMin.y + 1;
        // glfwSetCursorPos(glfwWindow, mousePos.x, mousePos.y - windowSize.y + 1);
    }
    else
    {
        // Do nothing since it's inside the boundaries.
        return a_currentPos;
    }
};

std::tuple<ImVec2, ImVec2> ImGuiUtils::getAbsouluteMinMaxRegionBoundaries()
{
    // These are the limits for the panel in absolute screen space coordinates
    // (it's affected by the window position)
    ImVec2 vMin = ImGui::GetWindowContentRegionMin();
    ImVec2 vMax = ImGui::GetWindowContentRegionMax();
    vMin.x += ImGui::GetWindowPos().x;
    vMin.y += ImGui::GetWindowPos().y;
    vMax.x += ImGui::GetWindowPos().x;
    vMax.y += ImGui::GetWindowPos().y;
    return {vMin, vMax};
}
void ImGuiUtils::wrapMouseAroundContentRegionFromMouseInput()
{
    ImVec2 mousePositionAbsolute = ImGui::GetMousePos();
    glm::vec2 mousePos = Input::getMousePosition();
    auto [vMin, vMax] = getAbsouluteMinMaxRegionBoundaries();
    ImVec2 windowSize(vMax.x - vMin.x, vMax.y - vMin.y);
    /*
    Debug info:
    ImGui::Text("vMin x: %f", vMin.x);
    ImGui::Text("vMin y: %f", vMin.y);
    ImGui::Text("vMax x: %f", vMax.x);
    ImGui::Text("vMax y: %f", vMax.y);
    ImGui::Text("windowSize x: %f", windowSize.x);
    ImGui::Text("windowSize y: %f", windowSize.y);
    ImGui::Text("mousePositionAbsolute x: %f", mousePositionAbsolute.x);
    ImGui::Text("mousePositionAbsolute y: %f", mousePositionAbsolute.y);
    ImGui::Text("mouse glfw x: %f", mousePos.x);
    ImGui::Text("mouse glfw y: %f", mousePos.y);
    */
    Window* window = &Application::getApp().getWindow();
    auto* glfwWindow = static_cast<GLFWwindow*>(window->getNativeWindow());

    ImVec2 newPos = wrapMouseAroundContentRegion(vMax, vMin, windowSize, mousePositionAbsolute,
                                                 ImVec2(mousePos.x, mousePos.y));
    glfwSetCursorPos(glfwWindow, newPos.x, newPos.y);
    highlightContentRegion();
}
void ImGuiUtils::highlightContentRegion()
{
    auto [vMin, vMax] = getAbsouluteMinMaxRegionBoundaries();
    ImGui::GetForegroundDrawList()->AddRect(vMin, vMax, IM_COL32(255, 255, 0, 255));
}

void ImGuiUtils::setDarkThemeColors()
{
    auto& colors = ImGui::GetStyle().Colors;
    colors[ImGuiCol_WindowBg] = ImVec4{0.1F, 0.105F, 0.11F, 1.0F};

    // Headers
    colors[ImGuiCol_Header] = ImVec4{0.2F, 0.205F, 0.21F, 1.0F};
    colors[ImGuiCol_HeaderHovered] = ImVec4{0.3F, 0.305F, 0.31F, 1.0F};
    colors[ImGuiCol_HeaderActive] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};

    // Buttons
    colors[ImGuiCol_Button] = ImVec4{0.2F, 0.205F, 0.21F, 1.0F};
    colors[ImGuiCol_ButtonHovered] = ImVec4{0.3F, 0.305F, 0.31F, 1.0F};
    colors[ImGuiCol_ButtonActive] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};

    // Frame BG
    colors[ImGuiCol_FrameBg] = ImVec4{0.2F, 0.205F, 0.21F, 1.0F};
    colors[ImGuiCol_FrameBgHovered] = ImVec4{0.3F, 0.305F, 0.31F, 1.0F};
    colors[ImGuiCol_FrameBgActive] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};

    // Tabs
    colors[ImGuiCol_Tab] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};
    colors[ImGuiCol_TabHovered] = ImVec4{0.38F, 0.3805F, 0.381F, 1.0F};
    colors[ImGuiCol_TabActive] = ImVec4{0.28F, 0.2805F, 0.281F, 1.0F};
    colors[ImGuiCol_TabUnfocused] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};
    colors[ImGuiCol_TabUnfocusedActive] = ImVec4{0.2F, 0.205F, 0.21F, 1.0F};

    // Title
    colors[ImGuiCol_TitleBg] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};
    colors[ImGuiCol_TitleBgActive] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};
    colors[ImGuiCol_TitleBgCollapsed] = ImVec4{0.15F, 0.1505F, 0.151F, 1.0F};
}