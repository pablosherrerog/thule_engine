#include <iostream>
#include <memory>

#include "thule_app/gui/imGuiLayer.hpp"
#include "thule_app/sandboxLayer.hpp"
#include "thule_app/simpleLayer.hpp"
#include "thule_engine/common/log.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/core/application.hpp"

int main()
{
    // Log::logLevel = Log::k_debug;  // uncomment to have debug traces
#ifdef WIN32
    Log::debug("Using Windows");
#endif
    try
    {
        PROFILER_SCOPED;
        Log::info("Using " + Utils::getStrCplusplusVersion(__cplusplus));
        Application thuleApp = Application("ThuleApp", 1280, 720);
        // std::unique_ptr<Layer> gameLayer{new SandboxLayer()};
        // thuleApp.pushLayer(std::make_unique<SimpleLayer>());

        thuleApp.pushLayer(std::make_unique<SandboxLayer>());
        thuleApp.pushLayer(std::make_unique<ImGuiLayer>());
        thuleApp.run();
    }
    catch (const std::exception& ex)
    {
        Log::error(ex.what());
        std::cout << "Press Enter to Exit" << std::endl;
        std::cin.ignore();
    }
}