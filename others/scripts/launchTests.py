
from pathlib import Path
import subprocess
import sys

root_path = Path(__file__).parent.parent.parent

linux_test_path = Path(root_path) / "build" / "thule_tests" /"thule_testing"
windows_test_path = linux_test_path.parent / "thule_testing.exe"
if linux_test_path.exists:
    test_path = linux_test_path
elif windows_test_path.exists:
    test_path = windows_test_path
else:
    raise ValueError(f"Error! test path {linux_test_path} doesn't seem to exist")
    sys.exit(1)

return_value = subprocess.call(test_path)
failed = bool(return_value)
if failed:
    print("Unit tests failed")
    sys.exit(1)

else:
    print("Unit test succeeded")
    sys.exit(0)

