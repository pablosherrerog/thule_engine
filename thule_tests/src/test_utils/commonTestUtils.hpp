#pragma once
#include <stdexcept>
#include <string>

class CommonTestUtils
{
   public:
    // This function is used instead of EXPECT_STREQ because e.what() includes the exception's path.
    static bool exceptionContainsString(const std::runtime_error &a_exception,
                                        const std::string &a_expectedText);
};