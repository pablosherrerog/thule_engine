#include "test_utils/commonTestUtils.hpp"

#include <iostream>
#include <stdexcept>
#include <string>

bool CommonTestUtils::exceptionContainsString(const std::runtime_error &a_exception,
                                              const std::string &a_expectedText)
{
    bool isSame = std::string(a_exception.what()).find(a_expectedText) != std::string::npos;
    if (!isSame)
    {
        std::cout << "----\n"
                  << "Test error! Received:\n"
                  << a_exception.what() << "\nwhich didn't contained:\n"
                  << a_expectedText << "\n----\n"
                  << std::endl;
    }
    return isSame;
}