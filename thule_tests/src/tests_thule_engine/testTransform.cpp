#include <gtest/gtest.h>

#include <limits>
#include <thule_engine/common/log.hpp>
#include <thule_engine/common/maths.hpp>
#include <thule_engine/geom/geomDef.hpp>

#include "glm/ext/quaternion_trigonometric.hpp"
#include "glm/fwd.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/trigonometric.hpp"
template <class T>
bool areVectorsEqual(const T &a_v1, const T &a_v2, float eps)
{
    return glm::all(glm::lessThan(glm::abs(a_v1 - a_v2), glm::vec3(eps)));
}
template <class T>
bool areVectorsEqual(const T &a_v1, const T &a_v2)
{
    return areVectorsEqual<T>(a_v1, a_v2, std::numeric_limits<float>::epsilon());
}

TEST(QuaterionBasicTest, BasicAssertions)
{
    vec3_t eulerAngles(90, 45, 0);
    quat_t orientation = Maths::eulerDegToQuaternion(eulerAngles);
    vec3_t eulerBackAgain = glm::degrees(glm::eulerAngles(orientation));
    EXPECT_TRUE(areVectorsEqual<glm::vec3>(eulerAngles, eulerBackAgain));
    // angleAxis require angle in radians and a a normalized rotation axis.
    quat_t quat1 = glm::angleAxis(glm::radians(90.0F), vec3_t(1, 0, 0));
    quat_t quat2 = glm::angleAxis(glm::radians(45.F), vec3_t(0, 1, 0));
    // The rotation is applied in the inverse order of the multiplication (quat1 is rotated by
    // quat2)
    quat_t finalQuat = quat2 * quat1;
    // mat4_t matrixRotation = glm::mat4_cast(finalQuat);
    vec3_t eulerFromAxisAngles = glm::degrees(glm::eulerAngles(finalQuat));
    EXPECT_TRUE(areVectorsEqual<glm::vec3>(eulerFromAxisAngles, eulerAngles, 0.001F));
}