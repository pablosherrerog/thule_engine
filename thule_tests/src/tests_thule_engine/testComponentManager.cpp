#include <gtest/gtest.h>

#include <stdexcept>
#include <string>
#include <thule_engine/common/log.hpp>
#include <thule_engine/components/transform.hpp>
#include <thule_engine/core/ecs/componentManager.hpp>

#include "test_utils/commonTestUtils.hpp"

TEST(TestExceptionRegisteringSameComponentTwice, BasicAssertions)
{
    ComponentManager componentManager;  // this tests _that_ the expected exception is thrown
    // https://stackoverflow.com/questions/23270078/test-a-specific-exception-type-is-thrown-and-the-exception-has-the-right-propert
    EXPECT_THROW(
        {
            try
            {
                componentManager.RegisterComponent<Transform>();
                componentManager.RegisterComponent<Transform>();
            }
            catch (const std::runtime_error& e)
            {
                std::string expectedText = "Component Transform was already registered";
                EXPECT_TRUE(CommonTestUtils::exceptionContainsString(e, expectedText));
                throw;
            }
        },
        std::runtime_error);
}

TEST(TestExceptionRegisterNonComponent, BasicAssertions)
{
    ComponentManager componentManager;  // this tests _that_ the expected exception is thrown
    // https://stackoverflow.com/questions/23270078/test-a-specific-exception-type-is-thrown-and-the-exception-has-the-right-propert
    EXPECT_THROW(
        {
            try
            {
                componentManager.GetComponentType<int>();
            }
            catch (const std::runtime_error& e)
            {
                EXPECT_TRUE(CommonTestUtils::exceptionContainsString(
                    e, "you are trying to register is not a component"));
                throw;
            }
        },
        std::runtime_error);
}
