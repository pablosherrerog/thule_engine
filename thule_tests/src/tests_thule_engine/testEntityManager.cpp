#include <gtest/gtest.h>

#include <thule_engine/common/log.hpp>
#include <thule_engine/core/ecs/entityManager.hpp>

TEST(TestCreateAndRemoveEntities, BasicAssertions)
{
    EntityManager entityManager;
    entityManager.createEntity();
    entityManager.createEntity();
    entityManager.createEntity();
    entityManager.removeEntity(1);
    entityManager.createEntity();
    int activeEntities = entityManager.getActiveEntities();
    EXPECT_EQ(activeEntities, 3);
    // EXPECT_EQ(activeEntities, 3) << "The remaining entities should be 3";
}
