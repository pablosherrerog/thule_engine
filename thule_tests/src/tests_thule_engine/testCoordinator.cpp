#include <gtest/gtest.h>

#include <string>
#include <thule_engine/common/log.hpp>
#include <thule_engine/core/ecs/coordinator.hpp>
#include <vector>

#include "thule_engine/components/metadata.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"

// Important! using preorder by itself is not going to identify uniquely the tree structure.
// But for this kind of tests should be enough.

void scene1Initialization(Coordinator& a_coordinator)
{
    /*
             A
            / \
           B   C
          /     \
         E       D
    */
    Entity a = a_coordinator.createEntity("A");
    Entity b = a_coordinator.createEntity(a, "B");
    a_coordinator.createEntity(b, "E");
    Entity c = a_coordinator.createEntity(a, "C");
    a_coordinator.createEntity(c, "D");
}

void scene2Initialization(Coordinator& a_coordinator)
{
    /*
             A
            / \
           B   E
          / \
         C   D
    */
    Entity a = a_coordinator.createEntity("A");
    Entity b = a_coordinator.createEntity(a, "B");
    a_coordinator.createEntity(b, "C");
    a_coordinator.createEntity(b, "D");
    a_coordinator.createEntity(a, "E");
}
template <typename T>
static void inline compareVectors(std::vector<T> a_vec1, std::vector<T> a_vec2)
{
    ASSERT_EQ(a_vec1.size(), a_vec2.size()) << "Vectors x and y are of unequal length";

    for (int i = 0; i < a_vec1.size(); ++i)
    {
        EXPECT_EQ(a_vec1[i], a_vec2[i]) << "Vectors x and y differ at index " << i;
    }
}

static std::vector<std::string> inline getNamesFromEntitiesVector(
    const std::vector<Entity>& a_entities, Coordinator& a_coordinator)
{
    std::vector<std::string> names;
    names.reserve(a_entities.size());
    for (Entity e : a_entities)
    {
        names.emplace_back(a_coordinator.getComponent<Metadata>(e).name);
    }
    return names;
}

TEST(TestPreOrder, BasicAssertions)
{
    Coordinator coordinator;
    scene1Initialization(coordinator);
    std::vector<Entity> entitiesOrdered;
    coordinator.getEntitiesFromTree(NULL_ENTITY, entitiesOrdered, Coordinator::k_preorder);
    std::vector<std::string> entitiesOrderedNames =
        getNamesFromEntitiesVector(entitiesOrdered, coordinator);
    std::vector<std::string> expectedNameOrder{"A", "B", "E", "C", "D"};
    compareVectors<std::string>(entitiesOrderedNames, expectedNameOrder);
}

TEST(TestPostOrder, BasicAssertions)
{
    Coordinator coordinator;
    scene2Initialization(coordinator);
    std::vector<Entity> entitiesOrdered;
    coordinator.getEntitiesFromTree(NULL_ENTITY, entitiesOrdered, Coordinator::k_postorder);
    std::vector<std::string> entitiesOrderedNames =
        getNamesFromEntitiesVector(entitiesOrdered, coordinator);
    std::vector<std::string> expectedNameOrder{"C", "D", "B", "E", "A"};
    compareVectors<std::string>(entitiesOrderedNames, expectedNameOrder);
}

TEST(TestSetEntityAsRootEntity, BasicAssertions)
{
    /*
             A
            / \
           B   C
          /     \
         E       D
    To
             A  C
            /    \
           B      D
          /
         E
    */
    Coordinator coordinator;
    scene1Initialization(coordinator);
    std::vector<Entity> entitiesOrdered;
    coordinator.setEntityAsRootEntity(3);  // Set C as root entity.
    coordinator.getEntitiesFromTree(NULL_ENTITY, entitiesOrdered, Coordinator::k_preorder);
    std::vector<std::string> entitiesOrderedNames =
        getNamesFromEntitiesVector(entitiesOrdered, coordinator);
    std::vector<std::string> expectedNameOrder{"A", "B", "E", "C", "D"};
    compareVectors<std::string>(entitiesOrderedNames, expectedNameOrder);
}

TEST(Test, BasicAssertions)
{
    /*
    Set C as child of B instead of A.
             A
            / \
           B   C
          /     \
         E       D
    To
             A
            /
           B
          / \
         E   C
              \
               D
    */
    Coordinator coordinator;
    scene1Initialization(coordinator);
    std::vector<Entity> entitiesOrdered;
    coordinator.setParentEntity(3, 2);  // Set C as child of B
    coordinator.getEntitiesFromTree(NULL_ENTITY, entitiesOrdered, Coordinator::k_preorder);
    std::vector<std::string> entitiesOrderedNames =
        getNamesFromEntitiesVector(entitiesOrdered, coordinator);
    std::vector<std::string> expectedNameOrder{"A", "B", "E", "C", "D"};
    compareVectors<std::string>(entitiesOrderedNames, expectedNameOrder);
}
